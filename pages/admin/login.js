import Layout from '../../components/admin/Layout';
import React from 'react';
import Link from 'next/link.js';
import Fetch from 'isomorphic-unfetch';
import { Cookies } from 'react-cookie';
import Router from 'next/router';


const serverUrl = 'http://localhost:3000';

// set up cookies
const cookies = new Cookies();
class login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: cookies.get('token') || null,
      username:'',
      password:'',
      token:''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange (event) {
    let field  = event.target.name
    if(field == "username"){
      this.setState({ username: event.target.value })
    } 
    else if(field == "password"){
      this.setState({ password: event.target.value })
    } 
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    const res =  await fetch(`${serverUrl}/api/admin/login`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({email:this.state.username, password:this.state.password})
    })

    const response = await res.json()

    if(response.success == true){
      console.log('response is',response);
      swal({
        title: "Login Sucessful!",
        text: " ",
        icon: "success",
        button: "ok",
      });
      Router.push({pathname: '/admin/dashboard'});
      const token = response.token;
      cookies.set('token', token);
      this.setState({token: token});
    }
    else{
      console.log('response is',response);
      swal({
        title: "Access Denied",
        text: response.msg ,
        icon: "warning",
        dangerMode: true,
      })
    }
  
  }

  render() {
return (
<Layout>
<div className="container-scroller">
  <div className="container-fluid page-body-wrapper full-page-wrapper auth-page">
    <div className="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
      <div className="row w-100">
        <div className="col-lg-4 mx-auto">
          <div className="auto-form-wrapper">
            <form onSubmit={(e)=> this.handleSubmit(e)}>
              <div className="form-group">
                <label className="label">Username</label>
                <div className="input-group">
                  <input name="username"  onChange={this.handleChange} type="text" className="form-control" placeholder="Username"/>
                  <div className="input-group-append">
                    <span className="input-group-text">
                      <i className="mdi mdi-check-circle-outline"></i>
                    </span>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label className="label">Password</label>
                <div className="input-group">
                  <input name="password" onChange={this.handleChange} type="password" className="form-control" placeholder="*********"/>
                  <div className="input-group-append">
                    <span className="input-group-text">
                      <i className="mdi mdi-check-circle-outline"></i>
                    </span>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-primary submit-btn btn-block" >Login</button>
              </div>
            </form>
          </div>
          <p className="footer-text text-center">copyright © 2019 Pharmbizz. All rights reserved.</p>
        </div>
      </div>
    </div>
     {/* content-wrapper ends  */}
  </div>
     {/* page-body-wrapper ends */}
</div>
</Layout>

    )
  }
}


export default login;