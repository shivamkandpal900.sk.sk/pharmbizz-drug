        import Layout from '../../../components/admin/Layout';
        import Header from '../../../components/admin/Header';
        import Sidebar from '../../../components/admin/Sidebar';
        import React, {Component} from 'react';
        import Swal from 'sweetalert2'
        // import 'sweetalert2/src/sweetalert2.scss'

        class drug extends React.Component {
            constructor(props){
            super(props);
            this.createClick = this.createClick.bind(this);
            this.state = {
                alert: null
            }    
        }

        createClick() {
          this.createClick = this.createClick.bind(this);
        }
  
        createClick = () => {
        (async function FormValues (){
    
          const {value: formValues} = await Swal.fire({
            title: 'Multiple inputs',
            html:
              '<input id="swal-input1" class="swal2-input">' +
              '<input id="swal-input2" class="swal2-input">',
            focusConfirm: false,
            preConfirm: () => {
              return [
                document.getElementById('swal-input1').value,
                document.getElementById('swal-input2').value
              ]
            }
          })
          
          if (formValues) {
            Swal.fire(JSON.stringify(formValues))
          }    
        })
      }
        
        

        delete() {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  swal(" Your data has been deleted!", {
                    icon: "success",
                  });
                } else {
                  swal("Your data is safe!");
                }
              });
            }
            
            

            render() {
                return (
            <Layout> 
            <div className="container-scroller">
            <Header/>
            <div className="container-fluid page-body-wrapper">
            <Sidebar/>



            <div className="main-panel">

            <div className="content-wrapper">
            {/* coading here  */}
            <div className="row">
                <div className="col-12 grid-margin">
                    <div className="card">
                    <div className="card-body">
                    <div className="col-12">
                        <span className="d-block d-md-flex align-items-center">
                        <button type="button" id='create' onClick= {() => this.createClick({ })} className="btn btn-primary"> Create </button>
                        </span>
                        </div>                   

                        <div className="fluid-container">
                        <div className="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
                        </div>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">DRUG_ID</th>
                                <th scope="col">DRUG_NAME</th>
                                <th scope="col">STUDY_NAME</th>
                                <th scope="col">COMPANY_ID</th>
                                <th scope="col">COUNTRY_ID</th>
                                <th> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <th scope="row">1</th>
                                <td>EB-101</td>
                                <td> </td>
                                <td> 1 </td>
                                <td> 1 </td>
                                <th> 
                                <button type="button" class="btn btn-icons btn-inverse-primary">
                                <i class="mdi  mdi-pencil"></i>
                                </button> &nbsp; &nbsp;
                                <button type="button" onClick= {() => this.delete()}  class="btn btn-icons btn-inverse-danger">
                                <i class="mdi  mdi-delete"></i>
                                {this.state.alert}
                                </button> 
                                </th>
                                </tr>
                                <tr>
                                <th scope="row">2</th>
                                <td>AB-102</td>
                                <td> </td>
                                <td>1</td>
                                <td>4</td>
                                <th> 
                                <button type="button" class="btn btn-icons btn-inverse-primary">
                                <i class="mdi  mdi-pencil"></i>
                                </button>  &nbsp; &nbsp;
                                <button type="button" onClick= {() => this.delete()}  class="btn btn-icons btn-inverse-danger">
                                <i class="mdi  mdi-delete"></i>
                                </button> 
                                </th>
                                </tr>

                                <tr>
                                <th scope="row">3 </th>
                                <td>AB-101</td>
                                <td> </td>
                                <td>1</td>
                                <td>3</td>
                                <th> 
                                <button type="button" class="btn btn-icons btn-inverse-primary">
                                <i class="mdi  mdi-pencil"></i>
                                </button> &nbsp; &nbsp;
                                <button type="button" onClick= {() => this.delete()}  class="btn btn-icons btn-inverse-danger">
                                <i class="mdi  mdi-delete"></i>
                                </button> 
                                </th>
                                </tr>
                                </tbody>
                            </table>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>

    </div>
</div>
</Layout>
)};
}

export default drug;