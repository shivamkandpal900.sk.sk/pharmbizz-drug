import Slider from '../components/Slider';
import Layout from '../components/Layout';
import Fetch from 'isomorphic-unfetch';
const index = ()=> (
<Layout>
<Slider />
{/* Main Content */}
<div className="wrapper"> 
    <div className="theme-padding">
        <div className="container">

           {/* Main Content Row */}
            <div className="row">

               {/* small sidebar */}
                
               {/* small sidebar */}

               {/* Content */}
                <div className="col-lg-7 col-md-7 col-sm-9 col-xs-8 r-full-width">

                   {/* Detail Post Widget */}
                    <div className="post-widget">
                        <div className="row">
                            <div className="col-xs-12">
                                {/* Secondary Heading */}
                                <div className="primary-heading">
                                    <h2>Biotech News</h2>
                                </div>
                               {/* Secondary Heading */}
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-20">

                               {/* Post */}
                                <div className="post style-2 white-bg light-shadow">

                                   {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="#" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                   {/* Post Img */}

                                   {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="listing-page-5.html">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                   {/* Post Detail */}

                                </div>
                               {/* Post */}

                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-20">
                               {/* Post */}
                                <div className="post style-2 white-bg light-shadow">

                                   {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="#" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                   {/* Post Img */}

                                   {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="listing-page-5.html">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                   {/* Post Detail */}

                                </div>
                               {/* Post */}

                            </div>
                            
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-20">

                               {/* Post */}
                                <div className="post style-2 white-bg light-shadow">

                                   {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="#" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                   {/* Post Img */}

                                   {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="listing-page-5.html">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                   {/* Post Detail */}

                                </div>
                               {/* Post */}

                            </div>
                            
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-20">

                               {/* Post */}
                                <div className="post style-2 white-bg light-shadow">

                                   {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="#" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                   {/* Post Img */}

                                   {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="listing-page-5.html">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                   {/* Post Detail */}

                                </div>
                               {/* Post */}

                            </div>
                        </div>                            
                    </div>
                   {/* Detail Post Widget */}
                  
                </div>
               {/* Content */}

               {/* Sidebar */}
                <div className="col-lg-3 col-md-3 col-xs-12 custom-re">
                    <div className="row">
                    <aside className="side-bar grid">
                       {/* Auther profile */}
                        <div className="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div className="widget light-shadow">
                                <h3 className="secondry-heading">Market Movers</h3>
                                <div className="auther-widget">
                                    <div className="responsive-img-box">
                                        <img src="/static/images/mm.jpg" />
                                    </div>
                                </div>
                            </div>
                        </div>
                       {/* Auther profile */}

                       {/* News Widget */}
                        <div className="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div className="widget">
                                <h3 className="secondry-heading">top news biotech</h3>
                                <div className="horizontal-tabs-widget">

                                   {/* tabs navs */}
                                    <ul className="theme-tab-navs">
                                        <li className="active">
                                            <a href="#week" data-toggle="tab">week</a>
                                        </li>
                                        <li>
                                            <a href="#month" data-toggle="tab">month</a>
                                        </li>
                                        <li>
                                            <a href="#all" data-toggle="tab">all</a>
                                        </li>
                                    </ul>
                                   {/* tabs navs */}
      
                                   {/* Tab panes */}
                                    <div className="horizontal-tab-content tab-content">
                                        <div className="tab-pane fade active in" id="week">
                                            <ul className="post-wrap-list">
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors it..</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                               <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors it..</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="tab-pane fade" id="month">
                                            <ul className="post-wrap-list">
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                               <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolor...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="tab-pane fade" id="all">
                                            <ul className="post-wrap-list">
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                               <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                   {/* Tab panes */}

                                </div>
                            </div>
                        </div>
                       {/* News Widget */}
                        
                        <div className="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div className="widget">
                                <ul className="aside-social">
                                    <li className="fb">
                                        <a href="#"><i className="fa fa-facebook"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li className="pi">
                                        <a href="#"><i className="fa fa-pinterest-p"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li className="tw">
                                        <a href="#"><i className="fa fa-twitter"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li className="gmail">
                                        <a href="#"><i className="fa fa-google-plus"></i><span>followers</span><em>100</em></a>
                                    </li>
                                     <li className="sky">
                                        <a href="#"><i className="fa fa-skype"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li className="yt">
                                        <a href="#"><i className="fa fa-youtube"></i><span>followers</span><em>100</em></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                    </div>
                </div>
               {/* Sidebar */}
                
                <div className="col-lg-2 col-md-2 col-sm-3 col-xs-4 r-hidden">

                   {/* add widget */}
                    <div className="aside-add">
                        <a href="#">
                            <img src="/static/images/aside-add.jpg" alt="" />
                        </a>
                    </div>
                   {/* add widget */}

                </div>
            
           </div>
           
           <div className="row">    
                <div className="col-md-8">
                    <div className="post-widget">
                        
                        <div className="post-widget">

                       {/* Main Heading */}
                        <div className="primary-heading">
                            <h2>Markets</h2>
                        </div>
                       {/* Main Heading */}

                       {/* List Post */}
                        <div className="p-30 light-shadow white-bg">
                            <div className="responsive-img-box">
                            <img src="/static/images/market1.jpg" />
                            </div>
                        </div>
                       {/* List Post */}

                    </div>

                    </div>
                </div>
                <div className="col-md-4">
                    <div className="post-widget">
                        
                        <div className="post-widget">

                       {/* Main Heading */}
                        <div className="primary-heading">
                            <h2>Future &amp; Commodities</h2>
                        </div>
                       {/* Main Heading */}

                       {/* List Post */}
                        <div className="p-30 light-shadow white-bg">
                            <div className="responsive-img-box">
                            <img src="/static/images/market2.jpg" />
                            </div>
                        </div>
                       {/* List Post */}

                    </div>

                    </div>
                </div>
           </div>
           
           <div className="row">    
                <div className="col-md-12">
                    <div className="post-widget">

                       {/* Heading */}
                        <div className="primary-heading">
                            <h2>Top Share Brokers</h2>
                        </div>
                       {/* Heading */}

                       {/* post slider */}
                        <div className="light-shadow white-bg p-30 slider-post"> 
                            <div id="post-slider-2">
                                <div className="post style-1">
                                    <div className="responsive-img-box">
                                        <img src="/static/images/share-broker-logo/logo-1.jpg" alt="" />
                                    </div>
                                </div>
                                <div className="post style-1">
                                    <div className="responsive-img-box">
                                        <img src="/static/images/share-broker-logo/logo-2.jpg" alt="" />
                                    </div>
                                </div>
                                 <div className="post style-1">
                                    <div className="responsive-img-box">
                                        <img src="/static/images/share-broker-logo/logo-3.jpg" alt="" />
                                    </div>
                                </div>
                                 <div className="post style-1">
                                    <div className="responsive-img-box">
                                        <img src="/static/images/share-broker-logo/logo-4.jpg" alt="" />
                                    </div>
                                </div>
                                 <div className="post style-1">
                                    <div className="responsive-img-box">
                                        <img src="/static/images/share-broker-logo/logo-5.jpg" alt="" />
                                    </div>
                                </div>
                            </div>

                        </div>
                       {/* post slider */}

                    </div>
                </div>
           </div>   
           {/* Main Content Row */}

        </div>
    </div>  

</div>
{/* main content */}
</Layout>
	);


export default index;