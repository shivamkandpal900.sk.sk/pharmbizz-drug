import Layout from '../components/Layout';


const post = (props) => (
<Layout>
<section className="banner-parallax overlay-dark" data-image-src="images/pharma-banner.jpg" data-parallax="scroll"> 
    <div className="inner-banner">
        <div className="container">
            <h3>NEWS DETAIL</h3>
            <ul className="tm-breadcrum">
                <li><a href="#">HOME</a></li>
                <li><a href="#">NEWS</a></li>
                <li>NEWS DETAIL</li>
            </ul>
        </div>
    </div>
</section>

{/* Main Content */}
<main className="main-wrap" id="post-detail"> 
        <div className="theme-padding">
            <div className="container">
                <div className="row">

                    <div className="col-md-9 col-sm-8">
                        <div className="content">

                            {/* blog detail */}
                            <div className="post-widget light-shadow white-bg">

                                {/* blog artical */}
                                <article className="post">
                                    
                                    {/* blog pot thumb */}
                                    <div className="post-thumb"> 
                                        <img src="/static/images/pharma-banner.jpg" alt="" />
                                    </div>
                                    {/* blog pot thumb */}

                                    {/* post detail */}
                                    <div className="post-info p-30">

                                        {/* title */}
                                        <h3>Quantum Secures $210 Million Long-Term Financing to Repay Existing Indebtedness and Provide Foundation for Growth</h3>
                                        {/* title */}

                                        {/* Post meta */}
                                        <div className="post_meta_holder">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    {/* post meta */}
                                                    <ul className="post-meta">
                                                        <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                        <li><i className="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                    {/* post meta */}
                                                </div>
                                                <div className="col-md-6">
                                                    {/* social icons */} 
                                                    <div className="blog-social">
                                                        <span className="share-icon btn-social-icon btn-adn"  data-toggle="tooltip" data-placement="top"    title="Sharing is Caring">
                                                            <span className="fa fa-share-alt"></span>
                                                        </span>
                                                        <ul>
                                                            <li>
                                                                <a className="btn-social-icon btn-facebook" href="#"  data-toggle="tooltip" data-placement="top" title="Share of Facebook">
                                                                    <span className="fa fa-facebook"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a className="btn-social-icon btn-twitter" href="#"  data-toggle="tooltip" data-placement="top" title="Post on Twitter">
                                                                    <span className="fa fa-twitter"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a className="btn-social-icon btn-pinterest" href="#"  data-toggle="tooltip" data-placement="top" title="Pin IT">
                                                                    <span className="fa fa-pinterest"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a className="btn-social-icon btn-linkedin" href="#"  data-toggle="tooltip" data-placement="top" title="Post on Linked In">
                                                                    <span className="fa fa-linkedin"></span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a className="btn-social-icon btn-vimeo" href="#"  data-toggle="tooltip" data-placement="top" title="Post on Twitter">
                                                                    <span className="fa fa-vimeo"></span>
                                                                </a>
                                                            </li>
                                                        </ul>                                                   
                                                    </div>
                                                    {/* social icons */} 
                                                </div>
                                            </div>
                                        </div>
                                        {/* Post meta */}
                                        {/* post description */}
                                        <div className="post-desc">
                                            <p>SAN JOSE, Calif., Dec. 28, 2018 /PRNewswire/ -- Quantum Corp. (NYSE: QTM) today announced the successful completion of a refinance of its current debt facilities.  The new $210 million of total credit facilities consist of a $165 million senior secured term loan facility, led by an investment fund managed by a leading US-based global fixed income asset manager, along with a $45 million revolving credit facility from the Company's current revolving credit facility lender.</p>
                                            <p>The new loan facility will be used to repay the Company's outstanding debt and related fees and interest (totaling $124.6 million with respect to its current term loan and $21.2 million with respect to its current revolving credit facility), provide capital to fund ongoing growth initiatives, and to support other general corporate purposes.  The Company's cash and cash equivalents as of the date of this release are approximately $14 million, and the amount drawn down under the new revolving credit facility is $4.4 million.</p>
                                            

                                            {/* blockqoute */}
                                            <blockquote className="qoute">
                                            <p> "This financing is a major milestone for Quantum and validates our credit-worthiness and business model outlook," says Jamie Lerner, Chairman and CEO.  "This facility, along with our other financial resources, positions us to execute our growth strategy.  We look forward to re-engaging with the investment community once we resolve the inherited challenges related to our previously announced restatement."</p>
                                            <div className="current-post-type">
                                                <i className="fa fa-quote-left"></i>
                                                </div>
                                            </blockquote>
                                            {/* blockqoute */}
                                            <h4>SEC Financial Statement Filings Status</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                            {/* thumbs */}
                                            <div className="row">
                                                <div className="col-md-4 col-sm-6 col-xs-4">
                                                    <div className="post-thumb">
                                                        <img src="/static/images/blog-detail/thumbs/img-01.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div className="col-md-4 col-sm-6 col-xs-4">
                                                    <div className="post-thumb">
                                                        <img src="/static/images/blog-detail/thumbs/img-02.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div className="col-md-4 hidden-sm col-xs-4">
                                                    <div className="post-thumb">
                                                        <img src="/static/images/blog-detail/thumbs/img-03.jpg" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                            {/* thumbs */}

                                             <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                        </div>
                                        {/* post description */}

                                        {/* tags and social icons */}
                                        <div className="row mb-20">

                                            {/* populer tags */} 
                                            <div className="col-md-6">
                                                <div className="blog-tags font-roboto">
                                                    <ul>
                                                        <li><a href="#">powerfull</a></li>
                                                        <li><a href="#">watch</a></li>
                                                        <li><a href="#">mobile</a></li>
                                                        <li><a href="#">tab</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            {/* populer tags */} 

                                            {/* social icons */} 
                                            <div className="col-md-6">
                                                <div className="blog-social">
                                                     <span className="share-icon btn-social-icon btn-adn"  data-toggle="tooltip" data-placement="top" title="Sharing is Caring">
                                                            <span className="fa fa-share-alt"></span>
                                                        </span>
                                                    <ul>
                                                        <li>
                                                            <a className="btn-social-icon btn-facebook" href="#"  data-toggle="tooltip" data-placement="top" title="Share of Facebook">
                                                                <span className="fa fa-facebook"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a className="btn-social-icon btn-twitter" href="#"  data-toggle="tooltip" data-placement="top" title="Post on Twitter">
                                                                <span className="fa fa-twitter"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a className="btn-social-icon btn-pinterest" href="#"  data-toggle="tooltip" data-placement="top" title="Pin IT">
                                                                <span className="fa fa-pinterest"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a className="btn-social-icon btn-linkedin" href="#"  data-toggle="tooltip" data-placement="top" title="Post on Linked In">
                                                                <span className="fa fa-linkedin"></span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a className="btn-social-icon btn-vimeo" href="#"  data-toggle="tooltip" data-placement="top" title="Post on Twitter">
                                                                <span className="fa fa-vimeo"></span>
                                                            </a>
                                                        </li>
                                                     </ul>                                                   
                                                </div>
                                            </div>
                                            {/* social icons */} 

                                        </div>
                                        {/* tags and social icons */}

                                    </div>

                                    {/* post detail */}

                                </article>
                                {/* blog artical */}
                               
                            </div>
                            {/* blog detail */}

                            {/* Slider Widget */}
                            <div className="post-widget">

                                {/* Heading */}
                                <div className="primary-heading">
                                    <h2>More Recent News</h2>
                                </div>
                                {/* Heading */}

                                {/* post slider */}
                                <div className="light-shadow gray-bg p-30"> 
                                    <div id="post-slider-2">

                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}
                                                
                                            </div>
                                            {/* thumbnail */}
                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}

                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}

                                            </div>
                                            {/* thumbnail */}

                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}

                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}

                                            </div>
                                            {/* thumbnail */}

                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}

                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}
                                                
                                            </div>
                                            {/* thumbnail */}
                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}

                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}

                                            </div>
                                            {/* thumbnail */}

                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}

                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                <span className="post-badge">post</span>

                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}

                                            </div>
                                            {/* thumbnail */}

                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}
                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                <span className="post-badge">post</span>

                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}
                                                
                                            </div>
                                            {/* thumbnail */}
                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}

                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                <span className="post-badge">post</span>

                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}

                                            </div>
                                            {/* thumbnail */}

                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}

                                        {/* post */}
                                        <div className="post style-1">

                                            {/* thumbnail */}
                                            <div className="post-thumb"> 
                                                <img src="/static/images/walmart.jpg" alt="" />
                                                <span className="post-badge">post</span>

                                                {/* post thumb hover */}
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                                {/* post thumb hover */}

                                            </div>
                                            {/* thumbnail */}

                                            <div className="post-content">
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i className="fa fa-thumbs-o-up"></i>20</li>
                                                </ul>
                                                <h5 className="m-0"><a href="#">Full Responsive amazing design And Pixel Perfect </a></h5>
                                            </div>
                                        </div>
                                        {/* post */}

                                    </div>
                                </div>
                                {/* post slider */}

                            </div>
                            {/* Slider Widget */}

                        </div>
                    </div>

                    {/* Sidebar */}
                    <div className="col-md-3 col-sm-4">
                        <aside className="side-bar">
                             {/* Catogires widget */}
                            <div className="widget">
                                <h3 className="secondry-heading">categories</h3>
                                <ul className="categories-widget">
                                    <li><a href="#"><em>fashion</em><span className="bg-green">15</span></a></li>
                                    <li><a href="#"><em>world</em><span className="bg-masterd">15</span></a></li>
                                    <li><a href="#"><em>technology</em><span className="bg-p-green">15</span></a></li>
                                    <li><a href="#"><em>health</em><span className="bg-orange">15</span></a></li>
                                    <li><a href="#"><em>lifestyle</em><span className="bg-gray">15</span></a></li>
                                    <li><a href="#"><em>sports</em><span className="bg-masterd">15</span></a></li>
                                  
                                </ul>
                            </div>
                            {/* Catogires widget */}
                            {/* News Widget */}
                            <div className="widget">
                                <h3 className="secondry-heading">top news</h3>
                                <div className="horizontal-tabs-widget">

                                    {/* tabs navs */}
                                    <ul className="theme-tab-navs">
                                        <li className="active">
                                            <a href="#week" data-toggle="tab">week</a>
                                        </li>
                                        <li>
                                            <a href="#month" data-toggle="tab">month</a>
                                        </li>
                                        <li>
                                            <a href="#all" data-toggle="tab">all</a>
                                        </li>
                                    </ul>
                                    {/* tabs navs */}
      
                                    {/* Tab panes */}
                                    <div className="horizontal-tab-content tab-content">
                                        <div className="tab-pane fade active in" id="week">
                                            <ul className="post-wrap-list">
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors it..</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                               <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors it..</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="tab-pane fade" id="month">
                                            <ul className="post-wrap-list">
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                               <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolor...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="tab-pane fade" id="all">
                                            <ul className="post-wrap-list">
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                               <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li className="post-wrap small-post">
                                                    <div className="post-thumb">
                                                      <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
                                                    </div>
                                                    <div className="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul className="post-meta">
                                                            <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i className="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    {/* Tab panes */}

                                </div>
                            </div>
                            {/* News Widget */}


                          {/* Social Networks */}
                            <div className="widget">
                                <ul className="aside-social">
                                    <li className="fb">
                                        <a href="#"><i className="fa fa-facebook"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li className="pi">
                                        <a href="#"><i className="fa fa-pinterest-p"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li className="tw">
                                        <a href="#"><i className="fa fa-twitter"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li className="gmail">
                                        <a href="#"><i className="fa fa-google-plus"></i><span>followers</span><em>100</em></a>
                                    </li>
                                     <li className="sky">
                                        <a href="#"><i className="fa fa-skype"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li className="yt">
                                        <a href="#"><i className="fa fa-youtube"></i><span>followers</span><em>100</em></a>
                                    </li>
                                </ul>
                            </div>
                            {/* Social Networks */}
                           

                        </aside>
                    </div>
                    {/* Sidebar */}

                </div>
            </div>
            {/* Content */}
        </div>       
    </main>
{/* main content */}


	</Layout>
	);
export default post;