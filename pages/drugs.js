import React from 'react';
import Layout from '../components/Layout';
import Fetch from 'isomorphic-unfetch';
import Head from 'next/head';
import Griddle, { plugins } from 'griddle-react';



var data = [
 {
   "Company": "Tiger Nixon",
   "Price": "System Architect",
   "Change": "Edinburgh",
   "Shares": "61",
   "Market Cap": "2011/04/25",
   "Short Ratio": "$320,800"
 },
 {
   "Company": "Garrett Winters",
   "Price": "Accountant",
   "Change": "Tokyo",
   "Shares": "63",
   "Market Cap": "2011/07/25",
   "Short Ratio": "$170,750"
 },
 {
   "Company": "Ashton Cox",
   "Price": "Junior Technical Author",
   "Change": "San Francisco",
   "Shares": "66",
   "Market Cap": "2009/01/12",
   "Short Ratio": "$86,000"
 },
 {
   "Company": "Cedric Kelly",
   "Price": "Senior Javascript Developer",
   "Change": "Edinburgh",
   "Shares": "22",
   "Market Cap": "2012/03/29",
   "Short Ratio": "$433,060"
 },
 {
   "Company": "Airi Satou",
   "Price": "Accountant",
   "Change": "Tokyo",
   "Shares": "33",
   "Market Cap": "2008/11/28",
   "Short Ratio": "$162,700"
 },
 {
   "Company": "Brielle Williamson",
   "Price": "Integration Specialist",
   "Change": "New York",
   "Shares": "61",
   "Market Cap": "2012/12/02",
   "Short Ratio": "$372,000"
 },
 {
   "Company": "Herrod Chandler",
   "Price": "Sales Assistant",
   "Change": "San Francisco",
   "Shares": "59",
   "Market Cap": "2012/08/06",
   "Short Ratio": "$137,500"
 },
 {
   "Company": "Rhona Davidson",
   "Price": "Integration Specialist",
   "Change": "Tokyo",
   "Shares": "55",
   "Market Cap": "2010/10/14",
   "Short Ratio": "$327,900"
 },
 {
   "Company": "Colleen Hurst",
   "Price": "Javascript Developer",
   "Change": "San Francisco",
   "Shares": "39",
   "Market Cap": "2009/09/15",
   "Short Ratio": "$205,500"
 },
 {
   "Company": "Sonya Frost",
   "Price": "Software Engineer",
   "Change": "Edinburgh",
   "Shares": "23",
   "Market Cap": "2008/12/13",
   "Short Ratio": "$103,600"
 },
 {
   "Company": "Jena Gaines",
   "Price": "Office Manager",
   "Change": "London",
   "Shares": "30",
   "Market Cap": "2008/12/19",
   "Short Ratio": "$90,560"
 },
 {
   "Company": "Quinn Flynn",
   "Price": "Support Lead",
   "Change": "Edinburgh",
   "Shares": "22",
   "Market Cap": "2013/03/03",
   "Short Ratio": "$342,000"
 },
 {
   "Company": "Charde Marshall",
   "Price": "Regional Director",
   "Change": "San Francisco",
   "Shares": "36",
   "Market Cap": "2008/10/16",
   "Short Ratio": "$470,600"
 },
 {
   "Company": "Haley Kennedy",
   "Price": "Senior Marketing Designer",
   "Change": "London",
   "Shares": "43",
   "Market Cap": "2012/12/18",
   "Short Ratio": "$313,500"
 },
 {
   "Company": "Tatyana Fitzpatrick",
   "Price": "Regional Director",
   "Change": "London",
   "Shares": "19",
   "Market Cap": "2010/03/17",
   "Short Ratio": "$385,750"
 },
 {
   "Company": "Michael Silva",
   "Price": "Marketing Designer",
   "Change": "London",
   "Shares": "66",
   "Market Cap": "2012/11/27",
   "Short Ratio": "$198,500"
 },
 {
   "Company": "Paul Byrd",
   "Price": "Chief Financial Officer (CFO)",
   "Change": "New York",
   "Shares": "64",
   "Market Cap": "2010/06/09",
   "Short Ratio": "$725,000"
 },
 {
   "Company": "Gloria Little",
   "Price": "Systems Administrator",
   "Change": "New York",
   "Shares": "59",
   "Market Cap": "2009/04/10",
   "Short Ratio": "$237,500"
 },
 {
   "Company": "Bradley Greer",
   "Price": "Software Engineer",
   "Change": "London",
   "Shares": "41",
   "Market Cap": "2012/10/13",
   "Short Ratio": "$132,000"
 },
 {
   "Company": "Dai Rios",
   "Price": "Personnel Lead",
   "Change": "Edinburgh",
   "Shares": "35",
   "Market Cap": "2012/09/26",
   "Short Ratio": "$217,500"
 },
 {
   "Company": "Jenette Caldwell",
   "Price": "Development Lead",
   "Change": "New York",
   "Shares": "30",
   "Market Cap": "2011/09/03",
   "Short Ratio": "$345,000"
 },
 {
   "Company": "Yuri Berry",
   "Price": "Chief Marketing Officer (CMO)",
   "Change": "New York",
   "Shares": "40",
   "Market Cap": "2009/06/25",
   "Short Ratio": "$675,000"
 },
 {
   "Company": "Caesar Vance",
   "Price": "Pre-Sales Support",
   "Change": "New York",
   "Shares": "21",
   "Market Cap": "2011/12/12",
   "Short Ratio": "$106,450"
 },
 {
   "Company": "Doris Wilder",
   "Price": "Sales Assistant",
   "Change": "Sidney",
   "Shares": "23",
   "Market Cap": "2010/09/20",
   "Short Ratio": "$85,600"
 },
 {
   "Company": "Angelica Ramos",
   "Price": "Chief Executive Officer (CEO)",
   "Change": "London",
   "Shares": "47",
   "Market Cap": "2009/10/09",
   "Short Ratio": "$1,200,000"
 },
 {
   "Company": "Gavin Joyce",
   "Price": "Developer",
   "Change": "Edinburgh",
   "Shares": "42",
   "Market Cap": "2010/12/22",
   "Short Ratio": "$92,575"
 },
 {
   "Company": "Jennifer Chang",
   "Price": "Regional Director",
   "Change": "Singapore",
   "Shares": "28",
   "Market Cap": "2010/11/14",
   "Short Ratio": "$357,650"
 },
 {
   "Company": "Brenden Wagner",
   "Price": "Software Engineer",
   "Change": "San Francisco",
   "Shares": "28",
   "Market Cap": "2011/06/07",
   "Short Ratio": "$206,850"
 },
 {
   "Company": "Company",
   "Price": "Price",
   "Change": "Change",
   "Shares": "Shares",
   "Market Cap": "Market Cap",
   "Short Ratio": "Short Ratio"
 }
];
const styleConfig = {
  classNames: {
    Table: 'table table-bordered table-striped table-hover',
    NextButton: 'btn btn-default ',
    PreviousButton:'btn btn-default',
    PageDropdown:"input-lg",
    Filter:"input-lg",
    SettingsToggle:'btn btn-default',
    Settings:'form-inline'
  },
   styles: {
    PageDropdown: { borderRadius: 0, margin:"10px" },
    Filter:{margin:"10px"},
    SettingsToggle: { borderRadius: 0, margin:"10px" },
    Settings: {margin:"10px"},
  }
}



const drugs = ()=> (
<Layout>
<div>
	<section className="pt-20 pb-20"> 
    <div className="inner-banner-ot">
        <div className="container">
        <div className="row">
            <div className="col-md-8">
				<div className="filter-form-outer">
					<video poster="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/polina.jpg" id="bgvid" playsInline autoPlay muted loop>
					 {/* 
					- Video needs to be muted, since Chrome 66+ will not autoplay video with sound.
					WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  */}
					<source src="http://thenewcode.com/assets/videos/polina.webm" type="video/webm" />
					<source src="http://thenewcode.com/assets/videos/polina.mp4" type="video/mp4" />
					</video>
					<div className="row">
						<div className="col-md-12">
							<div className="filter-form-box">
								<div className="pull-left">
									<div className="filter-input"> 
										<input type="search" name="search" id="search" placeholder="Search for company, drug, stage, etc..." />
									</div>
								</div>
								<div className="pull-right">
									<span className="color-white pr-20 display-inline-block">Showing 549 companies</span>
									<button type="button" className="btn btn-info filter-collapse-btn" data-toggle="collapse" data-target="#demo"><i className="fa fa-sliders"></i></button>
								    <span className="color-white pr-20 display-inline-block">SCREENER</span>
								</div>
								<div className="clear"></div>
							</div>
						</div>
						<div className="col-md-12">
							<div id="demo" className="collapse in">
								<div className="filter-form-bottom">
									<form>
										<div className="row">
											<div className="col-md-3">
												<div className="form-group">
													<label htmlFor="exampleInputEmail1" className="text-uppercase">Stage</label>
													<input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Any" />
												 </div>
											</div>
											<div className="col-md-3">
												<div className="form-group">
													<label htmlFor="exampleFormControlSelect1" className="text-uppercase">Market Cap</label>
													<select className="form-control" id="exampleFormControlSelect1">
													  <option>1</option>
													  <option>2</option>
													  <option>3</option>
													  <option>4</option>
													  <option>5</option>
													</select>
												  </div>
											</div>
											<div className="col-md-3">
												<div className="form-group">
													<label htmlFor="exampleFormControlSelect1" className="text-uppercase">Short Ratio</label>
													<select className="form-control" id="exampleFormControlSelect1">
													  <option>1</option>
													  <option>2</option>
													  <option>3</option>
													  <option>4</option>
													  <option>5</option>
													</select>
												  </div>
											</div>
											<div className="col-md-3">
												<div className="form-group">
													<label htmlFor="exampleFormControlSelect1" className="text-uppercase">Price to Book</label>
													<select className="form-control" id="exampleFormControlSelect1">
													  <option>1</option>
													  <option>2</option>
													  <option>3</option>
													  <option>4</option>
													  <option>5</option>
													</select>
												  </div>
											</div>
											<div className="col-md-3">
												<div className="form-group">
													<label htmlFor="exampleInputEmail1" className="text-uppercase">Indication</label>
													<input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Any" />
												 </div>
											</div>
											<div className="col-md-3">
												<div className="form-group">
													<label htmlFor="exampleInputEmail1" className="text-uppercase">Price</label>
													<select className="form-control" id="exampleFormControlSelect1">
													  <option>1</option>
													  <option>2</option>
													  <option>3</option>
													  <option>4</option>
													  <option>5</option>
													</select>
												 </div>
											</div>
											<div className="col-md-3">
												<div className="form-group">
													<label htmlFor="exampleInputEmail1" className="text-uppercase">Relative Volume</label>
													<select className="form-control" id="exampleFormControlSelect1">
													  <option>1</option>
													  <option>2</option>
													  <option>3</option>
													  <option>4</option>
													  <option>5</option>
													</select>
												 </div>
											</div>
											<div className="col-md-3">
												<label htmlFor="exampleInputEmail1" className="text-uppercase mt-25"></label>
												<div className="form-inline">
													<button type="submit" className="btn btn-primary filter-submit-btn">Apply Filter</button>
													<button type="submit" className="btn filter-reset-btn">Reset</button>
												 </div>
											</div>
										</div>	
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			  
				  
            </div>
            <div className="col-md-4">
            	<aside className="side-bar mt-20">
						<div className="widget">
							<h3 className="secondry-heading color-black">Graph Index</h3>
							<div className="horizontal-tabs-widget">
								<div className="p-20">
									<div className="responsive-img-box">
                                    	<img src="/static/images/graph-index.jpg" />
                                    </div>
								</div>
							</div>
						</div>
                 </aside>
            </div>
			
        </div>
    </div>
    </div>
</section>


{/* Main Content */}
<main className="wrapper"> 
    <div className="theme-padding pt-0">
        <div className="container">
                <div className="row">

                    <div className="col-md-8 col-sm-8">
						<div className="content">
							
							<div className="filter-data-table-box">
								<div className="p-20 light-shadow white-bg">
								  <Griddle
								  	styleConfig={styleConfig}
								    data={data}
								    plugins={[plugins.LocalPlugin]}
								  />
								</div>
							</div>
							
						   <div className="primary-heading mt-30">
								<h2>Biotech News</h2>
							</div>
							
							<div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-20">
                               {/* Post */}
                                <div className="post style-2 white-bg light-shadow">
                                   {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="#" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                   {/* Post Img */}
                                   {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="listing-page-5.html">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                   {/* Post Detail */}
                                </div>
                               {/* Post */}
                            </div>
							
							<div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-20">
                               {/* Post */}
                                <div className="post style-2 white-bg light-shadow">
                                   {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="#" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                   {/* Post Img */}
                                   {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="listing-page-5.html">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                   {/* Post Detail */}
                                </div>
                               {/* Post */}
                            </div>
							
							<div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-20">
                               {/* Post */}
                                <div className="post style-2 white-bg light-shadow">
                                   {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="#" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                   {/* Post Img */}
                                   {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="listing-page-5.html">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                   {/* Post Detail */}
                                </div>
                               {/* Post */}
                            </div>
							
							<div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-20">
                               {/* Post */}
                                <div className="post style-2 white-bg light-shadow">
                                   {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="#" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                   {/* Post Img */}
                                   {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="listing-page-5.html">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                   {/* Post Detail */}
                                </div>
                               {/* Post */}
                            </div>
							
						</div>
                    </div>

                   {/* Sidebar */}
                    <div className="col-md-4 col-sm-4">
                        <aside className="side-bar">
							
							<div className="widget">
                                <h3 className="secondry-heading">Top Today</h3>
                                <div className="horizontal-tabs-widget">
									<div className="p-20">
										<ul className="post-wrap-list">
											<li className="post-wrap small-post">
												<div className="post-thumb"> 
												  <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors it..</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
										   <li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors it..</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
											<li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors...</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
											<li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors it..</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
										   <li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors it..</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
											<li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors...</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
                                </div>
                            </div>
							
                            <div className="widget">
                                <h3 className="secondry-heading">Top Health News</h3>
                                <div className="horizontal-tabs-widget">
									<div className="p-20">
										<ul className="post-wrap-list">
											<li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors it..</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
										   <li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors it..</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
											<li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors...</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
											<li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-01.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors it..</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
										   <li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-02.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors it..</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
											<li className="post-wrap small-post">
												<div className="post-thumb">
												  <img src="/static/images/sidebar/news/img-03.jpg" alt="post" />
												</div>
												<div className="post-content">
													<h4><a href="#">Lorem ipsum dolors...</a></h4>
													<ul className="post-meta">
														<li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
														<li><i className="fa fa-comments-o"></i>20</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
                                </div>
                            </div>

                        </aside>
                    </div>
                   {/* Sidebar */}

                </div>
            </div>
    </div>  

</main>
{/* main content */}
</div>
	
</Layout>
);

export default drugs;