import React from 'react';
import Layout from '../components/Layout';
import ExampleChart from '../components/charts/Chart';
import fetch from 'isomorphic-unfetch';
import Router from 'next/router';

import Balance from '../components/financial/yearly/Balance';
import Income from '../components/financial/yearly/Income';
import Cash from '../components/financial/yearly/Cash';
import Calculation from '../components/financial/yearly/Calculation';

import BalanceQ from '../components/financial/quarterly/Balance';
import IncomeQ from '../components/financial/quarterly/Income';
import CashQ from '../components/financial/quarterly/Cash';

import * as d3 from 'd3-array';
import * as moment from 'moment';
import * as util from 'util';
import _ from 'lodash';



class stock extends React.Component {

   handleClick(query){
    let ticker = this.props.stock.ticker;
    this.setState({periodType: query})
     Router.push({
      pathname: '/stock',
      query: {period:query, ticker:ticker}
    });
   };

  static async getInitialProps({ query }) {
  let ticker = "AAPL";
  let period = "FY";

    if(query.period !== "" && query.period !== undefined ){
      period = query.period;
      //console.log("period is ", period);
    }

    if(query.ticker !== "" && query.ticker !== undefined ){
    ticker = query.ticker
    //console.log("ticker is ", ticker);
    }
    var stdFundamentalDataArr = {} ;
    var stdBalanceSheetDataArr ={};
    var stdcashflowDataArr = {};
    var stdCalculationsDataArr ={};
    var pricesArr = {currentPrice:'', profitPercentage:'', losePercentage:'', margin:'', date:'', volume:''};
    var fundamentalArr = [];
    var balanceArr = [];
    var cashflowArr = [];
    var calculationArr = [];
    var priceData = [];

    const key  = "?api_key=OmY1YjdjNDQ1YTg2N2YxY2E3ZDQxYjhkM2UyNWIyZTgx";
    const companyDetail = await fetch (`https://api-v2.intrinio.com/companies/${ticker}${key}`);
    const companyDetailData = await companyDetail.json();

    // company prices
    const livePrice = await fetch(`https://api-v2.intrinio.com/securities/${ticker}/prices/realtime${key}&source=iex`);
    const prices = await fetch(`https://api-v2.intrinio.com/securities/${ticker}/prices${key}`);
    const companyPriceData = await prices.json();
    const companyLivePriceData = await livePrice.json();

    let currentPrice = companyLivePriceData.last_price;
    let lastPrice = companyPriceData.stock_prices['0'].close;
    pricesArr['currentPrice'] = currentPrice;
    pricesArr['date'] = moment(companyLivePriceData.last_time).format("MMM Do YYYY");
    pricesArr['volume'] = companyLivePriceData.market_volume;

    if(currentPrice > lastPrice){
      let priceProfit = _.subtract(currentPrice, lastPrice);
      let priceProfitPercentage = _.floor( _.multiply(_.divide( priceProfit, lastPrice), 100 ), 2);
      pricesArr['margin'] = _.floor(priceProfit, 2);
      pricesArr['profitPercentage'] = priceProfitPercentage;
    }
    else if(currentPrice < lastPrice){
      let priceLose = _.subtract(lastPrice, currentPrice);
      let priceLosePercentage = _.floor(_.multiply(_.divide( priceLose, lastPrice), 100 ), 2);
      pricesArr['margin'] = _.floor(priceLose, 2);
      pricesArr['losePercentage'] = priceLosePercentage;
    }

     if(period == "QTY"){
      let year  = "2018"

    // income statement
    for (let i = 1; i < 5; i++) {
      let fundamental = await fetch (`https://api-v2.intrinio.com/fundamentals/lookup/${ticker}/income_statement/${year}/Q${i}${key}`);
      let fundamentalData =  await fundamental.json();
      let id  =  await fundamentalData.id;
      let stdFundamental = await fetch (`https://api-v2.intrinio.com/fundamentals/${id}/standardized_financials${key}`);
      stdFundamentalDataArr['Q'+i] =  await stdFundamental.json();
      }
      _.forEach(stdFundamentalDataArr['Q1'].standardized_financials, function(value, key) {
         fundamentalArr[key] =  {"Q1":stdFundamentalDataArr['Q1'].standardized_financials[key], "Q2":stdFundamentalDataArr['Q2'].standardized_financials[key], "Q3":stdFundamentalDataArr['Q3'].standardized_financials[key], "Q4":stdFundamentalDataArr['Q4'].standardized_financials[key]} ;
       });
        //console.log(util.inspect(stdFundamentalDataArr, false, null, true) );

    // balanceSheet
     for (let i = 1; i < 5; i++) {
      let balanceSheet = await fetch (`https://api-v2.intrinio.com/fundamentals/lookup/${ticker}/balance_sheet_statement/${year}/Q${i}${key}`);
      let balanceSheetData =  await balanceSheet.json();
      //console.log(balanceSheet);
      let id  =  await balanceSheetData.id;
      let stdBalanceSheet = await fetch (`https://api-v2.intrinio.com/fundamentals/${id}/standardized_financials${key}`);
      //console.log(stdBalanceSheet);
      stdBalanceSheetDataArr['Q'+i] =  await stdBalanceSheet.json();

      if(stdBalanceSheetDataArr['Q'+i].error){
        balanceSheet = await fetch (`https://api-v2.intrinio.com/fundamentals/lookup/${ticker}/balance_sheet_statement/${year}/FY${key}`);
        balanceSheetData =  await balanceSheet.json();
        let id  =  await balanceSheetData.id;
        stdBalanceSheet = await fetch (`https://api-v2.intrinio.com/fundamentals/${id}/standardized_financials${key}`);
        stdBalanceSheetDataArr['Q'+i] =  await stdBalanceSheet.json();
      }

      }
       _.forEach(stdBalanceSheetDataArr['Q1'].standardized_financials, function(value, key) {
         balanceArr[key] =  {"Q1":stdBalanceSheetDataArr['Q1'].standardized_financials[key], "Q2":stdBalanceSheetDataArr['Q2'].standardized_financials[key], "Q3":stdBalanceSheetDataArr['Q3'].standardized_financials[key], "Q4":stdBalanceSheetDataArr['Q4'].standardized_financials[key] } ;
       });
balanceArr['fundamental'] = {"Q1":stdBalanceSheetDataArr['Q1'].fundamental.end_date , "Q2":stdBalanceSheetDataArr['Q2'].fundamental.end_date ,"Q3":stdBalanceSheetDataArr['Q3'].fundamental.end_date,"Q4":stdBalanceSheetDataArr['Q4'].fundamental.end_date }
        //console.log(balanceArr);
       //console.log(util.inspect(stdBalanceSheetDataArr, false, null, true) );


      //cashFlow
      for (let i = 1; i < 5; i++) {
      let cashflow = await fetch (`https://api-v2.intrinio.com/fundamentals/lookup/${ticker}/cash_flow_statement/${year}/Q${i}${key}`);
      let cashflowData =  await cashflow.json();
      let id  =  await cashflowData.id;
      let stdcashflow = await fetch (`https://api-v2.intrinio.com/fundamentals/${id}/standardized_financials${key}`);
      stdcashflowDataArr['Q'+i] =  await stdcashflow.json();
      }
      _.forEach(stdcashflowDataArr['Q1'].standardized_financials, function(value, key) {
         cashflowArr[key] =  {"Q1":stdcashflowDataArr['Q1'].standardized_financials[key], "Q2":stdcashflowDataArr['Q2'].standardized_financials[key], "Q3":stdcashflowDataArr['Q3'].standardized_financials[key], "Q4":stdcashflowDataArr['Q4'].standardized_financials[key] } ;
       });
      //console.log(util.inspect(stdcashflowDataArr, false, null, true) );

     }

     else if(period == "FY"){

    // income statement
    for (let i = 2013; i < 2018; i++) {
      let fundamental = await fetch (`https://api-v2.intrinio.com/fundamentals/lookup/${ticker}/income_statement/${i}/${period}${key}`);
      let fundamentalData =  await fundamental.json();
      let id  =  await fundamentalData.id;
      let stdFundamental = await fetch (`https://api-v2.intrinio.com/fundamentals/${id}/standardized_financials${key}`);
      stdFundamentalDataArr[i] =  await stdFundamental.json();
      }
      _.forEach(stdFundamentalDataArr['2013'].standardized_financials, function(value, key) {
         fundamentalArr[key] =  {"oneThree":stdFundamentalDataArr['2013'].standardized_financials[key], "oneFour":stdFundamentalDataArr['2014'].standardized_financials[key], "oneFive":stdFundamentalDataArr['2015'].standardized_financials[key], "oneSix":stdFundamentalDataArr['2016'].standardized_financials[key], "oneSeven":stdFundamentalDataArr['2017'].standardized_financials[key]} ;
       });
        //console.log(util.inspect(fundamentalArr, false, null, true) );


     // balanceSheet
     for (let i = 2013; i < 2018; i++) {
      let balanceSheet = await fetch (`https://api-v2.intrinio.com/fundamentals/lookup/${ticker}/balance_sheet_statement/${i}/${period}${key}`);
      let balanceSheetData =  await balanceSheet.json();
      let id  =  await balanceSheetData.id;
      let stdBalanceSheet = await fetch (`https://api-v2.intrinio.com/fundamentals/${id}/standardized_financials${key}`);
      stdBalanceSheetDataArr[i] =  await stdBalanceSheet.json();
      //console.log("databalancesheet", JSON.stringify(stdBalanceSheetDataArr[i]) );
      }
       _.forEach(stdBalanceSheetDataArr['2013'].standardized_financials, function(value, key) {
         balanceArr[key] =  {"oneThree":stdBalanceSheetDataArr['2013'].standardized_financials[key], "oneFour":stdBalanceSheetDataArr['2014'].standardized_financials[key], "oneFive":stdBalanceSheetDataArr['2015'].standardized_financials[key], "oneSix":stdBalanceSheetDataArr['2016'].standardized_financials[key], "oneSeven":stdBalanceSheetDataArr['2017'].standardized_financials[key]} ;
       });
        // console.log(util.inspect(balanceArr, false, null, true) );

    //cashFlow
      for (let i = 2013; i < 2018; i++) {
      let cashflow = await fetch (`https://api-v2.intrinio.com/fundamentals/lookup/${ticker}/cash_flow_statement/${i}/FY${key}`);
      let cashflowData =  await cashflow.json();
      let id  =  await cashflowData.id;
      let stdcashflow = await fetch (`https://api-v2.intrinio.com/fundamentals/${id}/standardized_financials${key}`);
      stdcashflowDataArr[i] =  await stdcashflow.json();
      //console.log("datacashflow", JSON.stringify(stdcashflowDataArr[i]) );
      }
      _.forEach(stdcashflowDataArr['2017'].standardized_financials, function(value, key) {
         cashflowArr[key] =  {"oneThree":stdcashflowDataArr['2013'].standardized_financials[key], "oneFour":stdcashflowDataArr['2014'].standardized_financials[key], "oneFive":stdcashflowDataArr['2015'].standardized_financials[key], "oneSix":stdcashflowDataArr['2016'].standardized_financials[key], "oneSeven":stdcashflowDataArr['2017'].standardized_financials[key]} ;
       });
       //console.log(util.inspect(cashflowArr, false, null, true) );

    //calculations
      for (let i = 2013; i < 2018; i++) {
      let calculations = await fetch (`https://api-v2.intrinio.com/fundamentals/lookup/${ticker}/calculations/${i}/FY${key}`);
      let calculationsData =  await calculations.json();
      let id  =  await calculationsData.id;
      let stdCalculations = await fetch (`https://api-v2.intrinio.com/fundamentals/${id}/standardized_financials${key}`);
      stdCalculationsDataArr[i] =  await stdCalculations.json();
      //console.log("data", JSON.stringify(stdCalculationsDataArr[i]) );
      }
      _.forEach(stdCalculationsDataArr['2017'].standardized_financials, function(value, key) {
         calculationArr[key] =  {"oneThree":stdCalculationsDataArr['2013'].standardized_financials[key], "oneFour":stdCalculationsDataArr['2014'].standardized_financials[key], "oneFive":stdCalculationsDataArr['2015'].standardized_financials[key], "oneSix":stdCalculationsDataArr['2016'].standardized_financials[key], "oneSeven":stdCalculationsDataArr['2017'].standardized_financials[key]} ;
       });
       //console.log(util.inspect(calculation, false, null, true) );

      }


    return {
        stock: companyDetailData,
        prices: pricesArr,
        fundamental: fundamentalArr,
        balance: balanceArr,
        cashflow: cashflowArr,
        calculation: calculationArr,
        period: period,
    };
  }

  render() {
    return (
<Layout>
  <div>
<section className="banner-parallax innerpage_banner pt-20 pb-20">
    <div className="inner-banner">
        <div className="container">
        <div className="row">
            <div className="col-md-4">
                <div className="full company-info-title">
                    <div className="inner_page_info mt-0">
                        <h4 className="mt-10 text-left">{this.props.stock.legal_name} ( {this.props.stock.ticker} ) {this.props.stock.stock_exchange}</h4>
                        <div className="stock-price-up-down mt-10">
                          <div className="pull-left color-white"><i className="fa fa-dollar"></i> { this.props.prices.currentPrice}</div>

                           { this.props.prices.profitPercentage != '' ?(
                            <div className="pull-right color-green"> {this.props.prices.margin} ({this.props.prices.profitPercentage}%) <i className="fa fa-long-arrow-up"></i></div>
                            ):(
                             <div className="pull-right color-red"> {this.props.prices.margin} ({this.props.prices.losePercentage}%) <i className="fa fa-long-arrow-down"></i></div>
                            )
                            }
                      </div>
                        <div className="company-info-span">
                          <span className="text-left">* Delayed - data as of {this.props.prices.date}</span>
                           <p className="mb-0 text-uppercase text-left color-white">Volume: {this.props.prices.volume}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-8">
              <div id='dashboard'></div>
            </div>
        </div>
    </div>
    </div>
</section>

{/* Main Content */}
<main className="wrapper">
    <div className="theme-padding ">
        <div className="container">
                    <div className="col-md-9 col-sm-8">
                        <div className="content">
                            {/* review tabs */}
                            <div className="review-tabs-holder post-widget light-shadow p-30 white-bg">
                                <div className="horizontal-tabs-widget reviews-tabs pharm-nav-tab-box">

                                    {/* tabs navs */}
                                    <ul className="theme-tab-navs">
                                        <li className="active"><a href="#main" data-toggle="tab">Main</a></li>
                                        <li><a href="#company" data-toggle="tab">Company</a></li>
                                        <li><a href="#ownership" data-toggle="tab">Ownership</a></li>
                                        <li><a href="#financial" data-toggle="tab">Financial</a></li>
                                        <li><a href="#analysis" data-toggle="tab">Analysis</a></li>
                                        <li><a href="#news" data-toggle="tab">News</a></li>
                                    </ul>
                                    {/* tabs navs */}

                                    {/* Tab panes */}
                                    <div className="horizontal-tab-content tab-content">
                                        <div className="tab-pane fade active in" id="main">
                                            <h3 className="mt-10">Main</h3>
                                            <financialt> </financialt>
                                            <p className="tex-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <div className="responsive-img">
                                                <img src="/static/images/summary.jpg" />
                                            </div>

                                        </div>

                                        <div className="tab-pane fade" id="company">
                                            <h3>{this.props.stock.legal_name}</h3>
                                              <div className="col-md-6">
                                                <p className="tex-left"><a href="#">{this.props.stock.business_address}</a></p>
                                                <p className="tex-left"><a href="#">{this.props.stock.hq_country}</a></p>
                                                <p className="tex-left"><a href="#">{this.props.stock.business_phone_no}</a></p>
                                                <p className="tex-left"><a href="#">{this.props.stock.company_url}</a></p>
                                              </div>
                                              <div className="col-md-6">
                                                <p className="tex-left"><strong>Sector:</strong> <a href="#">{this.props.stock.sector}</a></p>
                                                <p className="tex-left"><strong>Industry:</strong> <a href="#">{this.props.stock.industry_category}</a></p>
                                              </div>
                                              <div className="clearfix"></div>
                                               <hr />
                                              <p className="tex-justify">{this.props.stock.long_description} </p>
                                        </div>

                                        <div className="tab-pane fade" id="ownership">
                                            <h3 className="mt-10">Ownership</h3>
                                            <p className="tex-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <div className="responsive-img">
                                                <img src="/static/images/summary.jpg" />
                                            </div>
                                        </div>
                                         {/*Financial Tab*/}
                                         <div className="tab-pane fade" id="financial">
                                          <div className="review-tabs-holder post-widget light-shadow p-30 white-bg">
                                              <div className="horizontal-tabs-widget reviews-tabs pharm-nav-tab-box">
                                                  <ul className="theme-tab-navs">
                                                      <li className="active"><a href="#income_statement" data-toggle="tab">Income Statement</a></li>
                                                      <li ><a href="#balances_sheet" data-toggle="tab">Balance Sheet</a></li>
                                                      <li><a href="#cash_flow" data-toggle="tab">Cash Flow</a></li>
                                                      { this.props.period == 'FY' ?(
                                                      <li><a href="#calculations" data-toggle="tab">Calculations</a></li>
                                                      ):(
                                                      <li></li>
                                                      )
                                                      }
                                                  </ul>
                                                   <p className="horizontal-tab-content">
                                                      <label className="radio-inline"> <input type="radio" id="FY" name="finRadio" value="FY" onClick={ (e)=>this.handleClick("FY") } /><strong>Yearly</strong></label>
                                                      <label className="radio-inline"><input type="radio" id= "Q1" name="finRadio" value="Q1" onClick={ (e)=>this.handleClick("QTY") } /><strong>Quarterly</strong></label>
                                                  </p>
                                                  <div className="horizontal-tab-content tab-content">
                                                      { this.props.period == 'FY' ?(
                                                        <div className="alert alert-info" role="alert">Annual Income Statement (values in 000's)</div>
                                                        ):(
                                                        <div className="alert alert-info" role="alert">Quarterly Income Statement (values in 000's)</div>
                                                        )
                                                      }
                                                      <div className="tab-pane fade active in" id="income_statement">
                                                        {/*income statement*/}
                                                        <div className="row chart_data">
                                                       { this.props.period == 'FY' ?(
                                                       <Income key="financial" financial={this.props.fundamental} name="oneThree" />
                                                       ):(
                                                       <IncomeQ key="financial" financial={this.props.fundamental} name="Q1" />
                                                         )
                                                        }

                                                        </div>
                                                         {/* ./income statement*/}
                                                      </div>
                                                    <div className="tab-pane fade" id="balances_sheet">
                                                      {/*Balance Sheet*/}
                                                        <div className="row chart_data">
                                                        { this.props.period == 'FY' ?(
                                                         <Balance key="balance" financial={this.props.balance} name="oneThree" />
                                                         ):(
                                                         <BalanceQ key="balance" financial={this.props.balance} name="Q1" />
                                                           )
                                                        }

                                                        </div>
                                                         {/*Balance Sheet*/}
                                                      </div>

                                                      <div className="tab-pane fade" id="cash_flow">
                                                        {/*Cash Flow*/}
                                                        <div className="row chart_data">
                                                       { this.props.period == 'FY' ?(
                                                       <Cash key="cashflow" financial={this.props.cashflow} name="oneSeven" />
                                                       ):(
                                                       <CashQ key="cashflow" financial={this.props.cashflow} name="Q1" />
                                                         )
                                                        }

                                                        </div>
                                                      {/* ./Cash Flow*/}
                                                      </div>
                                                      { this.props.period == 'FY' ?(
                                                      <div className="tab-pane fade" id="calculations">
                                                       {/*Calcutaions Sheet*/}
                                                        <div className="row chart_data">
                                                         <Calculation key="calculation" financial={this.props.calculation} name="oneThree" />
                                                        </div>
                                                         {/*Calculations Sheet*/}
                                                      </div>
                                                      ):(
                                                        <div ></div>
                                                      )
                                                      }
                                                  </div>
                                              </div>
                                          </div>
                                         </div>
                                        {/* ./Financial Tab*/}
                                        <div className="tab-pane fade" id="analysis">
                                            <h3 className="mt-10">Analysis</h3>
                                            <p className="tex-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <div className="responsive-img">
                                                <img src="/static/images/summary.jpg" />
                                            </div>
                                        </div>

                                        <div className="tab-pane fade" id="news">
                                            <h3 className="mt-10">News</h3>
                                            <p className="tex-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <div className="responsive-img">
                                                <img src="/static/images/summary.jpg" />
                                            </div>
                                        </div>

                                    </div>
                                    {/* Tab panes */}
                                </div>
                            </div>
                            {/* review tabs */}

                            {/* Slider Widget */}
                        </div>
                    </div>

                    {/* Sidebar */}
                    <div className="col-md-3 col-sm-4">
                        <aside className="side-bar">

                            <div className="widget">
                                <h3 className="secondry-heading">Charts</h3>
                                <div className="horizontal-tabs-widget">
                                    {/* tabs navs */}
                                    <ul className="theme-tab-navs style-2">
                                     {/*   <li className="active">
                                            <a href="#fixtures" data-toggle="tab">Latest</a>
                                        </li>
                                        <li>
                                            <a href="#result" data-toggle="tab">Past</a>
                                        </li>
                                      */}
                                    </ul>
                                    {/* tabs navs */}

                                    {/* Tab panes */}
                                    <div className="horizontal-tab-content tab-content">
                                        <div className="tab-pane fade active in" id="fixtures">
                                            <ExampleChart />
                                            {/*<svg className="chart"></svg>*/}
                                        </div>
                                        <div className="tab-pane fade" id="result">
                                            <svg className="chart"></svg>
                                        </div>
                                    </div>
                                    {/* Tab panes */}

                                </div>
                            </div>

                        </aside>
                    </div>
                    {/* Sidebar */}
            </div>
      </div>
   </main>
</div>
</Layout>

    )
  }
}

export default stock;
