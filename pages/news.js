import Layout from '../components/Layout';


const news = (props) => (
	<Layout>
<section className="banner-parallax overlay-dark" data-image-src="/static/images/pharma-banner.jpg" data-parallax="scroll"> 
    <div className="inner-banner">
    	<div className="container">
            <h3>NEWS</h3>
            <ul className="tm-breadcrum">
                <li><a href="#">HOME</a></li>
                <li>NEWS</li>
            </ul>
        </div>
    </div>
</section>

{/* Main Content */}
<main className="wrapper"> 
    <div className="theme-padding ">
        <div className="container">
                <div className="row">
                    <div className="col-md-8 col-sm-8">
                        <div className="row">
                        
                            <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-20">
                                {/* Post */}
                                <div className="post-thumb m-0 news-post-thumb">
                                    <img src="/static/images/walmart.jpg" alt="" />
                                     {/* image caption */}
                                    <div className="thumb-over">
                                        <h5><a href="news-detail.php">Very little is needed to make a happy life it is all within yourself, in your way of thinking.</a></h5>
                                        <ul className="post-meta">
                                             <li><i className="fa fa-clock-o"></i> 25 dec, 2016</li>
                                        </ul>
                                    </div>
                                    {/* image caption */}
                                </div>
                                {/* Post */}
                            </div>
                            
                            <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-20">
                                {/* Post */}
                                <div className="post-thumb m-0 news-post-thumb">
                                    <img src="/static/images/walmart.jpg" alt="" />
                                     {/* image caption */}
                                    <div className="thumb-over">
                                        <h5><a href="news-detail.php">Very little is needed to make a happy life it is all within yourself, in your way of thinking.</a></h5>
                                        <ul className="post-meta">
                                             <li><i className="fa fa-clock-o"></i> 25 dec, 2016</li>
                                        </ul>
                                    </div>
                                    {/* image caption */}
                                </div>
                                {/* Post */}
                            </div>
                            
                            <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-20">
                                {/* Post */}
                                <div className="post-thumb m-0 news-post-thumb">
                                    <img src="/static/images/walmart.jpg" alt="" />
                                     {/* image caption */}
                                    <div className="thumb-over">
                                        <h5><a href="news-detail.php">Very little is needed to make a happy life it is all within yourself, in your way of thinking.</a></h5>
                                        <ul className="post-meta">
                                             <li><i className="fa fa-clock-o"></i> 25 dec, 2016</li>
                                        </ul>
                                    </div>
                                    {/* image caption */}
                                </div>
                                {/* Post */}
                            </div>
                            
                            <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-20">
                                {/* Post */}
                                <div className="post style-2 white-bg light-shadow">
                                    {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="news-detail.php" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                    {/* Post Img */}
                                    {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="news-detail.php">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                    {/* Post Detail */}
                                </div>
                                {/* Post */}
                            </div>
                            
                            <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-20">
                                {/* Post */}
                                <div className="post style-2 white-bg light-shadow">
                                    {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="news-detail.php" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                    {/* Post Img */}
                                    {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="news-detail.php">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                    {/* Post Detail */}
                                </div>
                                {/* Post */}
                            </div>
                            
                            <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-20">
                                {/* Post */}
                                <div className="post style-2 white-bg light-shadow">
                                    {/* Post Img */}
                                    <div className="post-thumb">
                                        <img src="/static/images/technology/img-01.jpg" alt="detail" />
                                        <div className="thumb-hover">
                                            <div className="position-center-center">
                                                <a href="news-detail.php" className="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                    {/* Post Img */}
                                    {/* Post Detil */}
                                    <div className="post-content cat-listing">
                                        <h4><a href="news-detail.php">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                    {/* Post Detail */}
                                </div>
                                {/* Post */}
                            </div>
                          
                        </div>
                    </div>

                    {/* Sidebar */}
                    <div className="col-md-4 col-sm-4">
                        <aside className="side-bar">

                            <div className="widget">
                                <h3 className="secondry-heading">TOP NEWS</h3>
                                <div className="p-10">
                                    <ul className="post-wrap-list">
                                        <li className="post-wrap big-post">
                                            <div className="post-content">
                                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                 <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i> 25 dec, 2016 06:30 PM EST</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li className="post-wrap big-post">
                                            <div className="post-content">
                                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i> 25 dec, 2016 06:30 PM EST</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li className="post-wrap big-post">
                                            <div className="post-content">
                                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i> 25 dec, 2016 06:30 PM EST</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li className="post-wrap big-post">
                                            <div className="post-content">
                                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i> 25 dec, 2016 06:30 PM EST</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li className="post-wrap big-post">
                                            <div className="post-content">
                                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i> 25 dec, 2016 06:30 PM EST</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li className="post-wrap big-post">
                                            <div className="post-content">
                                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i> 25 dec, 2016 06:30 PM EST</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li className="post-wrap big-post">
                                            <div className="post-content">
                                                <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i> 25 dec, 2016 06:30 PM EST</li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                 </div>
                            </div>

                        </aside>
                    </div>
                    {/* Sidebar */}
                </div>
                
                <div className="row pt-20">
                	{/* Sidebar */}
                    <div className="col-md-3 col-sm-3">
                        <aside className="side-bar">
                            <div className="widget">
                                <h3 className="secondry-heading">INDUSTRIES</h3>
                                <ul className="categories-widget">
                                    <li><a href="#"><em>fashion</em><span className="bg-green">15</span></a></li>
                                    <li><a href="#"><em>world</em><span className="bg-masterd">15</span></a></li>
                                    <li><a href="#"><em>technology</em><span className="bg-p-green">15</span></a></li>
                                    <li><a href="#"><em>health</em><span className="bg-orange">15</span></a></li>
                                    <li><a href="#"><em>lifestyle</em><span className="bg-gray">15</span></a></li>
                                    <li><a href="#"><em>sports</em><span className="bg-masterd">15</span></a></li>
                                    <li><a href="#"><em>fashion</em><span className="bg-green">15</span></a></li>
                                    <li><a href="#"><em>world</em><span className="bg-masterd">15</span></a></li>
                                    <li><a href="#"><em>technology</em><span className="bg-p-green">15</span></a></li>
                                    <li><a href="#"><em>health</em><span className="bg-orange">15</span></a></li>
                                    <li><a href="#"><em>lifestyle</em><span className="bg-gray">15</span></a></li>
                                    <li><a href="#"><em>sports</em><span className="bg-masterd">15</span></a></li>
                                  
                                </ul>
                            </div>
                        </aside>
                    </div>
                    {/* Sidebar */}
                    <div className="col-md-9 col-sm-9">
                        <div className="post-widget">

                            {/* Main Heading */}
                            <div className="primary-heading primary-heading-with-btn-right">
                            	<div className="pull-left"><h2>BIOTECHNOLOGY</h2></div>
                                <div className="pull-right">
                                	<div className="dropdown">
                                      <button className="dropbtn">TOPICS <i className="fa fa-angle-down"></i></button>
                                      <div className="dropdown-content">
                                        <a href="#">Topics 1</a>
                                        <a href="#">Topics 2</a>
                                        <a href="#">Topics 3</a>
                                        <a href="#">Topics 4</a>
                                        <a href="#">Topics 5</a>
                                      </div>
                                    </div>
                                </div>
                                <div className="clear"></div>
                            </div>
                            {/* Main Heading */}

                            {/* List Post */}
                            <div className="p-30 light-shadow white-bg">
                                <ul className="list-posts latest-news-list-posts">
                                    <li className="row no-gutters">
                                        {/* thumbnail */}
                                        <div className="col-sm-4 col-xs-12">
                                            <div className="post-thumb"> 
                                                <img src="/static/images/pharma-comp-logo/logo-1.jpg" alt="" />
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* thumbnail */}
                                        {/* post detail */}
                                        <div className="col-sm-8 col-xs-12">
                                            <div className="post-content">
                                                <h4><a href="#">ZIVO Nutritional Efficacy Study for Poultry is Successful, Confirms Production Methods and Health Benefits</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {/* post detail */}
                                    </li>
                                    <li className="row no-gutters">
                                        {/* thumbnail */}
                                        <div className="col-sm-4 col-xs-12">
                                            <div className="post-thumb"> 
                                                <img src="/static/images/pharma-comp-logo/logo-2.jpg" alt="" />
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* thumbnail */}
                                        {/* post detail */}
                                        <div className="col-sm-8 col-xs-12">
                                            <div className="post-content">
                                                <h4><a href="#">Passage of U.S. Farm Bill a Major Milestone for EastWest’s Long Term Hemp Strategy</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {/* post detail */}
                                    </li>
                                    <li className="row no-gutters">
                                        {/* thumbnail */}
                                        <div className="col-sm-4 col-xs-12">
                                            <div className="post-thumb"> 
                                                <img src="/static/images/pharma-comp-logo/logo-3.jpg" alt="" />
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* thumbnail */}
                                        {/* post detail */}
                                        <div className="col-sm-8 col-xs-12">
                                            <div className="post-content">
                                                <h4><a href="#">Major Cancer Research Center Reports Pressure BioSciences' PCT Platform Could Play Major Role in Improving Cancer Diagnosis and Treatment</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {/* post detail */}
                                    </li>
                                    <li className="row no-gutters">
                                        {/* thumbnail */}
                                        <div className="col-sm-4 col-xs-12">
                                            <div className="post-thumb"> 
                                                <img src="/static/images/pharma-comp-logo/logo-4.jpg" alt="" />
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* thumbnail */}
                                        {/* post detail */}
                                        <div className="col-sm-8 col-xs-12">
                                            <div className="post-content">
                                                <h4><a href="#">
Eastwest BioScience Receives Multiple Purchase Orders with Global Retailer The TJX Companies Inc (NYSE: TJX) for Canada Wide Distribution</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {/* post detail */}
                                    </li>
                                    <li className="row no-gutters">
                                        {/* thumbnail */}
                                        <div className="col-sm-4 col-xs-12">
                                            <div className="post-thumb"> 
                                                <img src="/static/images/pharma-comp-logo/logo-1.jpg" alt="" />
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* thumbnail */}
                                        {/* post detail */}
                                        <div className="col-sm-8 col-xs-12">
                                            <div className="post-content">
                                                <h4><a href="#">ZIVO Nutritional Efficacy Study for Poultry is Successful, Confirms Production Methods and Health Benefits</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {/* post detail */}
                                    </li>
                                    <li className="row no-gutters">
                                        {/* thumbnail */}
                                        <div className="col-sm-4 col-xs-12">
                                            <div className="post-thumb"> 
                                                <img src="/static/images/pharma-comp-logo/logo-2.jpg" alt="" />
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* thumbnail */}
                                        {/* post detail */}
                                        <div className="col-sm-8 col-xs-12">
                                            <div className="post-content">
                                                <h4><a href="#">Passage of U.S. Farm Bill a Major Milestone for EastWest’s Long Term Hemp Strategy</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {/* post detail */}
                                    </li>
                                    <li className="row no-gutters">
                                        {/* thumbnail */}
                                        <div className="col-sm-4 col-xs-12">
                                            <div className="post-thumb"> 
                                                <img src="/static/images/pharma-comp-logo/logo-3.jpg" alt="" />
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* thumbnail */}
                                        {/* post detail */}
                                        <div className="col-sm-8 col-xs-12">
                                            <div className="post-content">
                                                <h4><a href="#">Major Cancer Research Center Reports Pressure BioSciences' PCT Platform Could Play Major Role in Improving Cancer Diagnosis and Treatment</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {/* post detail */}
                                    </li>
                                    <li className="row no-gutters">
                                        {/* thumbnail */}
                                        <div className="col-sm-4 col-xs-12">
                                            <div className="post-thumb"> 
                                                <img src="/static/images/pharma-comp-logo/logo-4.jpg" alt="" />
                                                <div className="thumb-hover">
                                                    <div className="position-center-center">
                                                        <a href="#" className="fa fa-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* thumbnail */}
                                        {/* post detail */}
                                        <div className="col-sm-8 col-xs-12">
                                            <div className="post-content">
                                                <h4><a href="#">
Eastwest BioScience Receives Multiple Purchase Orders with Global Retailer The TJX Companies Inc (NYSE: TJX) for Canada Wide Distribution</a></h4>
                                                <ul className="post-meta">
                                                    <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                                                </ul>
                                            </div>
                                        </div>
                                        {/* post detail */}
                                    </li>
                                </ul>
                            </div>
                            {/* List Post */}

                        </div>
                    </div>
                    
                </div>
               
            </div>
    </div>  

</main>
{/* main content */}


	</Layout>
	);
export default news;