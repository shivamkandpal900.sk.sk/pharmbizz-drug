'use strict';
const bcrypt = require('bcrypt-nodejs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    user_name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    user_type: DataTypes.STRING,
    token: DataTypes.TEXT,
    verify_key: DataTypes.TEXT
  }, {
      freezeTableName: true,
  });

  return User;
};