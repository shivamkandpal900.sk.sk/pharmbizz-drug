const express = require('express');
const router = express.Router();
const { User } = require('../models');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');



  router.post('/login', (req, res) => {

    User.findOne({ where: { email: req.body.email } }).then(async function (user) {
    console.log(user);
    if (!user) {
       res.send({success: false,  msg: 'Authentication failed. User not found.'});
    } else if(!bcrypt.compareSync(req.body.password || '', user.password)) {
        res.send({success: false,  msg: 'Wrong password.'});
    } else {
    	    const token = await jwt.sign(user.dataValues, 'SecrateKey');
          await user.update({token:'JWT '+ token});
          res.send({success: true, token: 'JWT ' + token});
    }

  })

});

   router.post('/register', async(req, res) => {
   	try {
   	let password = await bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8) );
  	  const userdata = {
              user_name:req.body.username,
              email: req.body.email,
              password: req.body.password
          }
        const user = await User.create(userdata)
        res.send({success: true, msg: 'Successful created new user.'})
   	
	   }catch(e){
	   	    return res.send({success: false, error:e, msg: 'please try again.'});
	   }

  });

  module.exports = router;