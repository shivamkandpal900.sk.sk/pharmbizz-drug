import React from 'react';
import { Chart } from "react-google-charts";
import _ from 'lodash';

class Income extends React.Component {
	 state = {
   	   dataLoadingStatus: 'loading',
   	   chartData: []
     };

   render() {

   	const financial = this.props.financial;
   	const name = this.props.name;
    //console.log('fin dta is ', financial);
   	const chartData = [];
   	const newChartData = [['#Quarter','1st','4th','3rd', '2nd']];

	 _.forEach(financial, function(fin) {
		let Q1 = _.result(fin, 'Q1.value')
		if( Math.abs(Q1) > 1000) Q1 = _.divide(Q1, 1000)

		let Q2 = _.result(fin, 'Q2.value')
		if( Math.abs(Q2) > 1000) Q2 = _.divide(Q2, 1000)

		let Q3 = _.result(fin, 'Q3.value')
		if( Math.abs(Q3) > 1000) Q3 = _.divide(Q3, 1000)

		let Q4 = _.result(fin, 'Q4.value')
		if( Math.abs(Q4) > 1000) Q4 = _.divide(Q4, 1000)

		chartData.push([ _.result(fin,name+'.data_tag.name'),  Q1, Q2, Q3, Q4])
	});

    _.forEach(chartData, function(chart) {
    	if(chart[0] == "Total Revenue") newChartData.push(chart)
    	else if(chart[0] == "Total Cost of Revenue") newChartData.push(chart)
    	else if(chart[0] == "Total Gross Profit") newChartData.push(chart)
    	else if(chart[0] == "Selling, General & Admin Expense") newChartData.push(chart)
    	else if(chart[0] == "Research & Development Expense") newChartData.push(chart)
    	else if(chart[0] == "Total Operating Expenses") newChartData.push(chart)
    	else if(chart[0] == "Total Operating Income") newChartData.push(chart)
    	else if(chart[0] == "Total Other Income / (Expense), net") newChartData.push(chart)
    	else if(chart[0] == "Total Pre-Tax Income") newChartData.push(chart)
    	else if(chart[0] == "Income Tax Expense") newChartData.push(chart)
    	else if(chart[0] == "Net Income / (Loss) Continuing Operations") newChartData.push(chart)
    	else if(chart[0] == "Consolidated Net Income / (Loss)") newChartData.push(chart)
    	else if(chart[0] == "Net Income / (Loss) Attributable to Common Shareholders") newChartData.push(chart)
    	else if(chart[0] == "Weighted Average Basic Shares Outstanding") newChartData.push(chart)
    	else if(chart[0] == "Diluted Earnings per Share") newChartData.push(chart)
        else if(chart[0] == "Weighted Average Basic & Diluted Shares Outstanding") newChartData.push(chart)
        else if(chart[0] == "Basic & Diluted Earnings per Share") newChartData.push(chart)
        else if(chart[0] == "Cash Dividends to Common per Share") newChartData.push(chart)
    });

      // console.log("New chart data is", newChartData );

	  this.state = {
	  dataLoadingStatus: 'ready',
	  chartData: newChartData,
	  };


  return this.state.dataLoadingStatus === 'ready' ? (
	<Chart
	  width={'100%'}
	  height={'100%'}
	  chartType="Table"
	  loader={<div>Loading Chart</div>}
	  data= {this.state.chartData}
	  formatters={[
		{
			  type: 'NumberFormat',
			  column: 1,
			  options: {
			    prefix: '$',
			    negativeColor: 'red',
			    negativeParens: true,
			    fractionDigits: 0,
			  },
			},
		{
		  type: 'NumberFormat',
		  column: 2,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		    {
		  type: 'NumberFormat',
		  column: 3,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 4,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 5,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},

	  ]}
	  options={{
	    allowHtml: true,
	    //showRowNumber: true,
	  }}
	  rootProps={{ 'data-testid': '4' }}
	/>
    ): (
      <div>Fetching data from API</div>
    )
    }
  }

export default Income
