import React from 'react';
import { Chart } from "react-google-charts";
import _ from 'lodash';

class Cash extends React.Component {
	 state = {
   	   dataLoadingStatus: 'loading', 
   	   chartData: []
     };

   render() { 

   	const financial = this.props.financial;
   	const name = this.props.name;
   	
   	const chartData =[ ];
   	const newChartData = [['#Quarter','1st','4th','3rd', '2nd']]; 
   
	 _.forEach(financial, function(fin) {
		let Q1 = _.result(fin, 'Q1.value')
		if( Math.abs(Q1) > 1000) Q1 = _.divide(Q1, 1000)

		let Q2 = _.result(fin, 'Q2.value')
		if( Math.abs(Q2) > 1000) Q2 = _.divide(Q2, 1000)

		let Q3 = _.result(fin, 'Q3.value')
		if( Math.abs(Q3) > 1000) Q3 = _.divide(Q3, 1000)

		let Q4 = _.result(fin, 'Q4.value')
		if( Math.abs(Q4) > 1000) Q4 = _.divide(Q4, 1000)

		chartData.push([ _.result(fin,name+'.data_tag.name'),  Q1, Q2, Q3, Q4])
	});

	 _.forEach(chartData, function(chart) {
    	if(chart[0] == "Depreciation Expense") newChartData.push(chart)
    	else if(chart[0] == "Non-Cash Adjustments To Reconcile Net Income") newChartData.push(chart)
    	else if(chart[0] == "Accounts Receivable") newChartData.push(chart)
    	else if(chart[0] == "Changes in Inventories") newChartData.push(chart)
    	else if(chart[0] == "Changes in Operating Assets and Liabilities, net") newChartData.push(chart)
    	else if(chart[0] == "Net Cash From Operating Activities") newChartData.push(chart)
    	else if(chart[0] == "Purchase of Property, Plant & Equipment") newChartData.push(chart)
    	else if(chart[0] == "Purchase of Investments") newChartData.push(chart)
    	else if(chart[0] == "Sale and/or Maturity of Investments") newChartData.push(chart)
    	else if(chart[0] == "Other Investing Activities, net") newChartData.push(chart)
    	else if(chart[0] == "Net Cash From Investing Activities") newChartData.push(chart)
    	else if(chart[0] == "Repayment of Debt") newChartData.push(chart)
    	else if(chart[0] == "Repurchase of Common Equity") newChartData.push(chart)
    	else if(chart[0] == "Payment of Dividends") newChartData.push(chart)
    	else if(chart[0] == "Issuance of Debt") newChartData.push(chart)
     	else if(chart[0] == "Issuance of Common Equity") newChartData.push(chart)
    	else if(chart[0] == "Other Financing Activities, net") newChartData.push(chart)
    	else if(chart[0] == "Cash Interest Paid") newChartData.push(chart)
    	else if(chart[0] == "Cash Income Taxes Paid") newChartData.push(chart)
    	else if(chart[0] == "Net Change in Cash & Equivalents") newChartData.push(chart)
    	else if(chart[0] == "Net Cash From Continuing Financing Activities") newChartData.push(chart)
    	else if(chart[0] == "Net Cash From Financing Activities") newChartData.push(chart)
    });


	  this.state = {
	  dataLoadingStatus: 'ready',
	  chartData: newChartData,
	  };


  return this.state.dataLoadingStatus === 'ready' ? (
	<Chart
	  setCell = {22, 2, 15, 'Fifteen', {style: 'font-style:bold; font-size:22px;'}}
	  width={'100%'}
	  height={'100%'}
	  chartType="Table"
	  loader={<div>Loading Chart</div>}
	  data= {this.state.chartData}
	  formatters={[
		{
			  type: 'NumberFormat',
			  column: 1,
			  options: {
			    prefix: '$',
			    negativeColor: 'red',
			    negativeParens: true,
			    fractionDigits: 0,
			  },
			},
		{
		  type: 'NumberFormat',
		  column: 2,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		    {
		  type: 'NumberFormat',
		  column: 3,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 4,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 5,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
	  ]}
	  options={{
	    allowHtml: true,
	    //showRowNumber: true,
	  }}
	  rootProps={{ 'data-testid': '3' }}
	/>
    ): (
      <div>Fetching data from API</div>
    )
    }
  }

export default Cash