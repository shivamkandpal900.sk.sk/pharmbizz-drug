import React from 'react';
import { Chart } from "react-google-charts";
import _ from 'lodash';

class Balance extends React.Component {
	 state = {
   	   dataLoadingStatus: 'loading',
   	   chartData: []
     };

   render() {

   	const financial = this.props.financial;
   	const name = this.props.name;
		//console.log("financialdata",financial.fundamental['Q1']);

   	const chartData =[ ];
		const newChartData = [['#Quarter', financial.fundamental['Q1'], financial.fundamental['Q4'], financial.fundamental['Q3'], financial.fundamental['Q2'] ]];

	 _.forEach(financial, function(fin) {
		let Q1 = _.result(fin, 'Q1.value')
		if( Math.abs(Q1) > 1000) Q1 = _.divide(Q1, 1000)

		let Q2 = _.result(fin, 'Q2.value')
		if( Math.abs(Q2) > 1000) Q2 = _.divide(Q2, 1000)

		let Q3 = _.result(fin, 'Q3.value')
		if( Math.abs(Q3) > 1000) Q3 = _.divide(Q3, 1000)

		let Q4 = _.result(fin, 'Q4.value')
		if( Math.abs(Q4) > 1000) Q4 = _.divide(Q4, 1000)

		chartData.push([ _.result(fin,name+'.data_tag.name'),  Q1, Q2, Q3, Q4])
	});

	 _.forEach(chartData, function(chart) {
	 	if(chart[0] == "Cash & Equivalents") newChartData.push(chart)
    	else if(chart[0] == "Short-Term Investments") newChartData.push(chart)
    	else if(chart[0] == "Note & Lease Receivable") newChartData.push(chart)
    	else if(chart[0] == "Accounts Receivable") newChartData.push(chart)
    	else if(chart[0] == "Inventories, net") newChartData.push(chart)
    	else if(chart[0] == "Other Current Assets") newChartData.push(chart)
    	else if(chart[0] == "Total Current Assets") newChartData.push(chart)
     	else if(chart[0] == "Plant, Property, & Equipment, net") newChartData.push(chart)
    	else if(chart[0] == "Long-Term Investments") newChartData.push(chart)
    	else if(chart[0] == "Goodwill") newChartData.push(chart)
    	else if(chart[0] == "Intangible Assets") newChartData.push(chart)
    	else if(chart[0] == "Other Noncurrent Operating Assets") newChartData.push(chart)
    	else if(chart[0] == "Total Noncurrent Assets") newChartData.push(chart)
    	else if(chart[0] == "Total Assets") newChartData.push(chart)
    	else if(chart[0] == "Short-Term Debt") newChartData.push(chart)
    	else if(chart[0] == "Accounts Payable") newChartData.push(chart)
    	else if(chart[0] == "Accrued Expenses") newChartData.push(chart)
     	else if(chart[0] == "Total Current Liabilities") newChartData.push(chart)
    	else if(chart[0] == "Long-Term Debt") newChartData.push(chart)
    	else if(chart[0] == "Other Noncurrent Operating Liabilities") newChartData.push(chart)
    	else if(chart[0] == "Total Liabilities") newChartData.push(chart)
    	else if(chart[0] == "Common Stock") newChartData.push(chart)
    	else if(chart[0] == "Retained Earnings") newChartData.push(chart)
    	else if(chart[0] == "Treasury Stock") newChartData.push(chart)
    	else if(chart[0] == "Total Preferred & Common Equity") newChartData.push(chart)
    	else if(chart[0] == "Total Liabilities & Shareholders' Equity") newChartData.push(chart)

    });


	  this.state = {
	  dataLoadingStatus: 'ready',
	  chartData: newChartData,
	  };


  return this.state.dataLoadingStatus === 'ready' ? (
	<Chart
	  width={'100%'}
	  height={'100%'}
	  chartType="Table"
	  loader={<div>Loading Chart</div>}
	  data= {this.state.chartData}
	  formatters={[
		{
			  type: 'NumberFormat',
			  column: 1,
			  options: {
			    prefix: '$',
			    negativeColor: 'red',
			    negativeParens: true,
			    fractionDigits: 0,
			  },
			},
		{
		  type: 'NumberFormat',
		  column: 2,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		    {
		  type: 'NumberFormat',
		  column: 3,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 4,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 5,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
	  ]}
	  options={{
	    allowHtml: true,
	    //showRowNumber: true,
	  }}
	  rootProps={{ 'data-testid': '1' }}
	/>
    ): (
      <div>Fetching data from API</div>
    )
    }
  }



export default Balance
