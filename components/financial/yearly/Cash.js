import React from 'react';
import { Chart } from "react-google-charts";
import _ from 'lodash';

class Cash extends React.Component {
	 state = {
   	   dataLoadingStatus: 'loading', 
   	   chartData: []
     };

   render() { 

   	const financial = this.props.financial;
   	const name = this.props.name;
   	
   	const chartData =[ ];
   const newChartData = [['#Fiscal Year','2013','2014','2015', '2016', '2017']]; 
   
	 _.forEach(financial, function(fin) {
		let oneThree = _.result(fin, 'oneThree.value')
		if(Math.abs(oneThree) > 1000) oneThree = _.divide(oneThree, 1000)

		let oneFour = _.result(fin, 'oneFour.value')
		if( Math.abs(oneFour) > 1000) oneFour = _.divide(oneFour, 1000)

		let oneFive = _.result(fin, 'oneFive.value')
		if( Math.abs(oneFive) > 1000) oneFive = _.divide(oneFive, 1000)

		let oneSix = _.result(fin, 'oneSix.value')
		if( Math.abs(oneSix) > 1000) oneSix = _.divide(oneSix, 1000)

		let oneSeven = _.result(fin, 'oneSeven.value')
		if( Math.abs(oneSeven) > 1000) oneSeven = _.divide(oneSeven, 1000)

		chartData.push([ _.result(fin,name+'.data_tag.name'),  oneThree, oneFour, oneFive, oneSix, oneSeven])
	});

	 _.forEach(chartData, function(chart) {
    	if(chart[0] == "Depreciation Expense"){
    		//newChartData.push(['Cash Flows-Operating Activities', , , , , ])
    		newChartData.push(chart)
    	}
    	else if(chart[0] == "Non-Cash Adjustments To Reconcile Net Income") newChartData.push(chart)
    	
    	else if(chart[0] == "Accounts Receivable") {
    		//newChartData.push(["Changes in Operating Activities","","","","",""])
    		newChartData.push(chart)
    	}
    	else if(chart[0] == "Changes in Inventories") newChartData.push(chart)
    	else if(chart[0] == "Changes in Operating Assets and Liabilities, net") newChartData.push(chart)
    	else if(chart[0] == "Net Cash From Operating Activities") newChartData.push(chart)
    	
    	else if(chart[0] == "Purchase of Property, Plant & Equipment") {
    		//newChartData.push(["Cash Flows-Investing Activities","","","","",""])
    		newChartData.push(chart)
    	}
    	else if(chart[0] == "Purchase of Investments") newChartData.push(chart)
    	else if(chart[0] == "Sale and/or Maturity of Investments") newChartData.push(chart)
    	else if(chart[0] == "Other Investing Activities, net") newChartData.push(chart)
    	else if(chart[0] == "Net Cash From Investing Activities") newChartData.push(chart)
    	else if(chart[0] == "Repayment of Debt") {
    		//newChartData.push(["Cash Flows-Financing Activities","","","","",""])
    		newChartData.push(chart)
    	}
    	else if(chart[0] == "Repurchase of Common Equity") newChartData.push(chart)
    	else if(chart[0] == "Payment of Dividends") newChartData.push(chart)
    	else if(chart[0] == "Issuance of Debt") newChartData.push(chart)
     	else if(chart[0] == "Issuance of Common Equity") newChartData.push(chart)
    	else if(chart[0] == "Other Financing Activities, net") newChartData.push(chart)
    	else if(chart[0] == "Cash Interest Paid") newChartData.push(chart)
    	else if(chart[0] == "Cash Income Taxes Paid") newChartData.push(chart)
    	else if(chart[0] == "Net Change in Cash & Equivalents") newChartData.push(chart)
    	else if(chart[0] == "Net Cash From Continuing Financing Activities") newChartData.push(chart)
    	else if(chart[0] == "Net Cash From Financing Activities") newChartData.push(chart)
    });


	  this.state = {
	  dataLoadingStatus: 'ready',
	  chartData: newChartData,
	  };


  return this.state.dataLoadingStatus === 'ready' ? (
	<Chart
	  setCell = {22, 2, 15, 'Fifteen', {style: 'font-style:bold; font-size:22px;'}}
	  width={'100%'}
	  height={'100%'}
	  chartType="Table"
	  loader={<div>Loading Chart</div>}
	  data= {this.state.chartData}
	  formatters={[
		{
			  type: 'NumberFormat',
			  column: 1,
			  options: {
			    prefix: '$',
			    negativeColor: 'red',
			    negativeParens: true,
			    fractionDigits: 0,
			  },
			},
		{
		  type: 'NumberFormat',
		  column: 2,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		    {
		  type: 'NumberFormat',
		  column: 3,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 4,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 5,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
	  ]}
	  options={{
	    allowHtml: true,
	    //showRowNumber: true,
	  }}
	  rootProps={{ 'data-testid': '3' }}
	/>
    ): (
      <div>Fetching data from API</div>
    )
    }
  }

export default Cash