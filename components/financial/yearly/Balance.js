import React from 'react';
import { Chart } from "react-google-charts";
import _ from 'lodash';

class Balance extends React.Component {
	 state = {
   	   dataLoadingStatus: 'loading', 
   	   chartData: []
     };

   render() { 

   	const financial = this.props.financial;
   	const name = this.props.name;
   	
   	const chartData =[ ];
   	const newChartData = [['#Fiscal Year','2013','2014','2015', '2016', '2017']]; 
   
	 _.forEach(financial, function(fin) {
		let oneThree = _.result(fin, 'oneThree.value')
		if( Math.abs(oneThree) > 1000) oneThree = _.divide(oneThree, 1000)

		let oneFour = _.result(fin, 'oneFour.value')
		if( Math.abs(oneFour) > 1000) oneFour = _.divide(oneFour, 1000)

		let oneFive = _.result(fin, 'oneFive.value')
		if( Math.abs(oneFive) > 1000) oneFive = _.divide(oneFive, 1000)

		let oneSix = _.result(fin, 'oneSix.value')
		if( Math.abs(oneSix) > 1000) oneSix = _.divide(oneSix, 1000)

		let oneSeven = _.result(fin, 'oneSeven.value')
		if( Math.abs(oneSeven) > 1000) oneSeven = _.divide(oneSeven, 1000)

		chartData.push([ _.result(fin,name+'.data_tag.name'),  oneThree, oneFour, oneFive, oneSix, oneSeven])
	});

	 _.forEach(chartData, function(chart) {
	 	if(chart[0] == "Cash & Equivalents") {
    	//newChartData.push(["ASSETS","","","","",""])	
    	//newChartData.push(["CURRENT ASSETS","","","","",""])	
    	newChartData.push(chart)
    	}
    	else if(chart[0] == "Short-Term Investments") newChartData.push(chart)
    	else if(chart[0] == "Note & Lease Receivable") newChartData.push(chart)
    	else if(chart[0] == "Accounts Receivable") newChartData.push(chart)
    	else if(chart[0] == "Inventories, net") newChartData.push(chart)
    	else if(chart[0] == "Other Current Assets") newChartData.push(chart)
    	else if(chart[0] == "Total Current Assets") newChartData.push(chart)

     	else if(chart[0] == "Plant, Property, & Equipment, net") {
     		//newChartData.push(["LONG TERM ASSETS","","","","",""])
     		newChartData.push(chart)
     	}
    	else if(chart[0] == "Long-Term Investments") newChartData.push(chart)
    	else if(chart[0] == "Goodwill") newChartData.push(chart)
    	else if(chart[0] == "Intangible Assets") newChartData.push(chart)
    	else if(chart[0] == "Other Noncurrent Operating Assets") newChartData.push(chart)
    	else if(chart[0] == "Total Noncurrent Assets") newChartData.push(chart)
    	else if(chart[0] == "Total Assets") newChartData.push(chart)
    	
    	else if(chart[0] == "Short-Term Debt") { 
    		//newChartData.push(["LIABILITIES","","","","",""])
    		newChartData.push(chart)
    	}
    	else if(chart[0] == "Accounts Payable") newChartData.push(chart)
    	else if(chart[0] == "Accrued Expenses") newChartData.push(chart)
     	else if(chart[0] == "Total Current Liabilities") newChartData.push(chart)
    	else if(chart[0] == "Long-Term Debt") newChartData.push(chart)
    	else if(chart[0] == "Other Noncurrent Operating Liabilities") newChartData.push(chart)
    	else if(chart[0] == "Total Liabilities") newChartData.push(chart)

    	else if(chart[0] == "Common Stock"){
    	//newChartData.push(["SHARE HOLDERS EQUITY","","","","",""])
    	newChartData.push(chart)
    	}
    	else if(chart[0] == "Retained Earnings") newChartData.push(chart)
    	else if(chart[0] == "Treasury Stock") newChartData.push(chart)
    	else if(chart[0] == "Total Preferred & Common Equity") newChartData.push(chart)
    	else if(chart[0] == "Total Liabilities & Shareholders' Equity") newChartData.push(chart)
    });


	  this.state = {
	  dataLoadingStatus: 'ready',
	  chartData: newChartData,
	  };


  return this.state.dataLoadingStatus === 'ready' ? (
	<Chart
	  width={'100%'}
	  height={'100%'}
	  chartType="Table"
	  loader={<div>Loading Chart</div>}
	  data= {this.state.chartData}
	  formatters={[
		{
			  type: 'NumberFormat',
			  column: 1,
			  options: {
			    prefix: '$',
			    negativeColor: 'red',
			    negativeParens: true,
			    fractionDigits: 0,
			  },
			},
		{
		  type: 'NumberFormat',
		  column: 2,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		    {
		  type: 'NumberFormat',
		  column: 3,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 4,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 5,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
	  ]}
	  options={{
	    allowHtml: true,
	    //showRowNumber: true,
	  }}
	  rootProps={{ 'data-testid': '1' }}
	/>
    ): (
      <div>Fetching data from API</div>
    )
    }
  }



export default Balance