import React from 'react';
import { Chart } from "react-google-charts";
import _ from 'lodash';

class Income extends React.Component {
	 state = {
   	   dataLoadingStatus: 'loading', 
   	   chartData: []
     };

   render() { 

   	const financial = this.props.financial;
   	const name = this.props.name;
   	
   	const chartData = [];
   	const newChartData = [['#Fiscal Year','2013','2014','2015', '2016', '2017']]; 
   
	 _.forEach(financial, function(fin) {
		let oneThree = _.result(fin, 'oneThree.value')
		if( Math.abs(oneThree) > 1000) oneThree = _.divide(oneThree, 1000)

		let oneFour = _.result(fin, 'oneFour.value')
		if( Math.abs(oneFour) > 1000) oneFour = _.divide(oneFour, 1000)

		let oneFive = _.result(fin, 'oneFive.value')
		if( Math.abs(oneFive) > 1000) oneFive = _.divide(oneFive, 1000)

		let oneSix = _.result(fin, 'oneSix.value')
		if( Math.abs(oneSix) > 1000) oneSix = _.divide(oneSix, 1000)

		let oneSeven = _.result(fin, 'oneSeven.value')
		if( Math.abs(oneSeven) > 1000) oneSeven = _.divide(oneSeven, 1000)

		chartData.push([ _.result(fin,name+'.data_tag.name'),  oneThree, oneFour, oneFive, oneSix, oneSeven])
	});
    
    _.forEach(chartData, function(chart) {
    	if(chart[0] == "Total Revenue") newChartData.push(chart)
    	else if(chart[0] == "Total Cost of Revenue") newChartData.push(chart)
    	else if(chart[0] == "Total Gross Profit") newChartData.push(chart)
    	else if(chart[0] == "Selling, General & Admin Expense") newChartData.push(chart)
    	else if(chart[0] == "Research & Development Expense") newChartData.push(chart)
    	else if(chart[0] == "Total Operating Expenses") newChartData.push(chart)
    	else if(chart[0] == "Total Operating Income") newChartData.push(chart)
    	else if(chart[0] == "Total Other Income / (Expense), net") newChartData.push(chart)
    	else if(chart[0] == "Total Pre-Tax Income") newChartData.push(chart)
    	else if(chart[0] == "Income Tax Expense") newChartData.push(chart)
    	else if(chart[0] == "Net Income / (Loss) Continuing Operations") newChartData.push(chart)
    	else if(chart[0] == "Consolidated Net Income / (Loss)") newChartData.push(chart)
    	else if(chart[0] == "Net Income / (Loss) Attributable to Common Shareholders") newChartData.push(chart)
    	else if(chart[0] == "Weighted Average Basic Shares Outstanding") newChartData.push(chart)
    	else if(chart[0] == "Diluted Earnings per Share") newChartData.push(chart)
        else if(chart[0] == "Weighted Average Basic & Diluted Shares Outstanding") newChartData.push(chart)
        else if(chart[0] == "Basic & Diluted Earnings per Share") newChartData.push(chart)
        else if(chart[0] == "Cash Dividends to Common per Share") newChartData.push(chart)
    });

      //console.log("New chart data is", newChartData );

	  this.state = {
	  dataLoadingStatus: 'ready',
	  chartData: newChartData,
	  };


  return this.state.dataLoadingStatus === 'ready' ? (
	<Chart
	  width={'100%'}
	  height={'100%'}
	  chartType="Table"
	  setCell = { [2, 5, 12, {style: 'font-style:bold; font-size:72px;'}] }
	  loader={<div>Loading Chart</div>}
	  data= {this.state.chartData}
	  formatters={[
		{
			  type: 'NumberFormat',
			  column: 1,
			  options: {
			    prefix: '$',
			    negativeColor: 'red',
			    negativeParens: true,
			    fractionDigits: 0,
			  },
			},
		{
		  type: 'NumberFormat',
		  column: 2,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		    {
		  type: 'NumberFormat',
		  column: 3,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 4,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
		{
		  type: 'NumberFormat',
		  column: 5,
		  options: {
		    prefix: '$',
		    negativeColor: 'red',
		    negativeParens: true,
		    fractionDigits: 0,
		  },
		},
	  ]}
	  options={{
	    allowHtml: true,
	    //showRowNumber: true,
	  }}
	  rootProps={{ 'data-testid': '4' }}
	/>
    ): (
      <div>Fetching data from API</div>
    )
    }
  }

export default Income