import * as React from "react";
import { Chart } from "react-google-charts";

const data=[
    ['Company', 'Financial'],
    ['2013', 33],
    ['2014', 26],
    ['2015', 22],
    ['2016', 10], 
    ['2017', 9], 
  ];
const options = {
    title: 'Company Financial',
    sliceVisibilityThreshold: 0.2,
};
const ExampleChart = () => {
  return (
    <Chart
      chartType="PieChart"
      data={data}
      options={options}
    />
  );
};

export default ExampleChart;