import Head from 'next/head';
import Navbar from './Navbar';
import Footer from './Footer';

const Layout = (props) =>(
<div>
         <Head>
		<meta charSet="utf-8" />
		<title>pharmbizz</title>
		<link rel="stylesheet" href="/static/css/normalize.css" />
		<link rel="stylesheet" href="/static/css/bootstrap/bootstrap.min.css" />
		<link rel="stylesheet" href="/static/css/plugin.css" />
		<link rel="stylesheet" href="/static/css/animate.css" />
		<link rel="stylesheet" href="/static/css/transition.css" />
		<link rel="stylesheet" href="/static/css/icomoon.css" />
		<link rel="stylesheet" href="/static/css/style.css" />
		<link rel="stylesheet" href="/static/css/color-1.css" />
		<link rel="stylesheet" href="/static/css/responsive.css" />
		<link rel="stylesheet" href="/static/css/font-awesome.min.css" />
		<link rel="stylesheet" href="https://unpkg.com/react-bootstrap-typeahead/css/Typeahead.css" />
		{/* Switcher CSS */}
		<link rel="stylesheet" href="/static/switcher/switcher.css" rel="stylesheet" type="text/css"/> 
		<link rel="stylesheet" id="skins" href="/static/css/color-1.css" type="text/css" media="all" />
		<link rel="stylesheet" href="/static/css/suggestion-box.min.css" />
		<link rel="stylesheet" href="/static/css/carouselTicker.css" />
		<link rel="stylesheet" href="/static/css/margin-padding.css" type="text/css" media="all" />
		<link rel="stylesheet" href="/static/css/custom-style.css" type="text/css" media="all" />
		{/* FontsOnline */}
		<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300italic,300,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css' />

        <script src="/static/js/vendor/jquery.js"></script>
        <script src="/static/js/vendor/bootstrap.min.js"></script>
        <script src="/static/js/datepicker.js"></script> 
		<script src="/static/js/parallax.js"></script>
		<script src="/static/js/prettyPhoto.js"></script>
		<script src="/static/js/isotope.pkgd.js"></script> 
		<script src="/static/js/jquery.carouselTicker.min.js"></script> 
		<script src="/static/js/scrollbar.js"></script>
		<script src="/static/js/owl-carousel.js"></script>
        <script src="/static/js/bxslider.js"></script> 
        <script src="/static/js/big-slide.js"></script>
        <script src="/static/js/main.js"></script>
        <script src="/static/js/suggestion-box.min.js"></script>

     </Head>

     <Navbar/>
     {props.children}
     <Footer/>


</div>
				
);
export default Layout;