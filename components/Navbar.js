import React from 'react'
import Link from 'next/link'
import Search from './Search'
const Navbar = ()=> (
      <div className="header-wrap">
      <div className="top-bar">
      <div className="container">
      <div className="row">

      {/* Top Left Nav */}
      <div className="col-sm-6 col-xs-6">
      <ul className="top-left">
      <li></li>
      <li></li>
      </ul>
      </div>
      {/* Top Left Nav */}

      {/* Top Right Nav */}
      <div className="col-sm-6 col-xs-6 r-full-width">
      <ul className="top-right text-right">
      <li className="md-trigger" data-modal="login-popup"><a href="#"><i className="fa fa-key">
      </i>login</a></li>
      <li className="md-trigger" data-modal="register-popup"><a href="#"><i className="icon-user"></i>register</a></li>

      </ul>
      </div>
      {/* Top Right Nav */}

      </div>
      </div>
      </div>
      {/* Navigation Holder */}
      <header className="header header-2">
      <div className="container">
      <div className="nav-holder">

      {/* logo */}
      <div className="logo-holder">
      <a href="index.php" className="inner-logo-2"></a>
      </div>
      {/* logo */}


      {/*search  Top Right Nav */}


      <div className="pull-right">
      <div className="search">
       <Search />
      </div>
      </div>

      {/* Top Right Nav */}

      {/* Navigation*/}
      <div className="cr-navigation">

      {/* Navbar*/}
      <nav className="cr-nav">
      <ul>
      <li>
         <Link href="/"><a>HOME</a></Link>
      </li>
      <li>
         <Link href="/news"><a>NEWS</a></Link>
      </li>
      <li>
          <Link href="/stock"><a>STOCK</a></Link>
      </li>
      <li>
         <Link href="/drugs"><a>DRUGS</a></Link>
      </li>
      <li><a href="#">FUTURES</a></li>
      <li><a href="#">CALENDER</a></li>
      <li><a href="#">TOOLS</a></li>
      <li><a href="#">MARKETPLACE</a></li>
      </ul>
      </nav>
      {/* Navbar*/}

      {/* Secondry Nav*/}
      <ul className="cr-add-nav">
      <li><a href="#menu" className="menu-link"><i className="fa fa-bars"></i></a></li>
      </ul>
      {/* Secondry Nav*/}

      </div>
      {/* Navigation*/}

      </div>
      </div>
      </header>
      {/* Navigation Holder*/}

      {/* Stock Market Ticker*/}
      <section id="main-content" className="">
               <div id="demos">
                  <div id="carouselTicker" className="carouselTicker">
                     <ul className="carouselTicker__list">
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Bitcoin<span className="update_change_minus">-1,521.25</span>
                                 </div>
                                 <div className="coin_price">
                                    $11,459.75<span className="scsl__change_minus">-12.97%</span>
                                 </div>
                                 <div className="coin_time">
                                    $175,016,158,112.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Ethereum<span className="update_change_minus">-109.12</span>
                                 </div>
                                 <div className="coin_price">
                                    $952.98<span className="scsl__change_minus">-11.45%</span>
                                 </div>
                                 <div className="coin_time">
                                    $92,587,551,437.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Exchange Union<span className="update_change_minus">-0.33</span>
                                 </div>
                                 <div className="coin_price">
                                    $8.16<span className="scsl__change_minus">-4.02%</span>
                                 </div>
                                 <div className="coin_time">
                                    $16,322,520.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Ripple<span className="update_change_minus">-0.14</span>
                                 </div>
                                 <div className="coin_price">
                                    $1.25<span className="scsl__change_minus">-11.05%</span>
                                 </div>
                                 <div className="coin_time">
                                    $48,231,782,365.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Veritaseum<span className="update_change_minus">-46.70</span>
                                 </div>
                                 <div className="coin_price">
                                    $337.46<span className="scsl__change_minus">-13.84%</span>
                                 </div>
                                 <div className="coin_time">
                                    $687,292,480.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Digitalcoin<span className="update_change_minus">-0.01</span>
                                 </div>
                                 <div className="coin_price">
                                    $0.07<span className="scsl__change_minus">-14.89%</span>
                                 </div>
                                 <div className="coin_time">
                                    $1,986,979.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Bitcoin<span className="update_change_minus">-1,521.25</span>
                                 </div>
                                 <div className="coin_price">
                                    $11,459.75<span className="scsl__change_minus">-12.97%</span>
                                 </div>
                                 <div className="coin_time">
                                    $175,016,158,112.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Ethereum<span className="update_change_minus">-109.12</span>
                                 </div>
                                 <div className="coin_price">
                                    $952.98<span className="scsl__change_minus">-11.45%</span>
                                 </div>
                                 <div className="coin_time">
                                    $92,587,551,437.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Exchange Union<span className="update_change_minus">-0.33</span>
                                 </div>
                                 <div className="coin_price">
                                    $8.16<span className="scsl__change_minus">-4.02%</span>
                                 </div>
                                 <div className="coin_time">
                                    $16,322,520.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Ripple<span className="update_change_minus">-0.14</span>
                                 </div>
                                 <div className="coin_price">
                                    $1.25<span className="scsl__change_minus">-11.05%</span>
                                 </div>
                                 <div className="coin_time">
                                    $48,231,782,365.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Veritaseum<span className="update_change_minus">-46.70</span>
                                 </div>
                                 <div className="coin_price">
                                    $337.46<span className="scsl__change_minus">-13.84%</span>
                                 </div>
                                 <div className="coin_time">
                                    $687,292,480.00
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li className="carouselTicker__item">
                           <div className="coin_info">
                              <div className="inner">
                                 <div className="coin_name">
                                    Digitalcoin<span className="update_change_minus">-0.01</span>
                                 </div>
                                 <div className="coin_price">
                                    $0.07<span className="scsl__change_minus">-14.89%</span>
                                 </div>
                                 <div className="coin_time">
                                    $1,986,979.00
                                 </div>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </section>
      </div>
    );

export default Navbar
