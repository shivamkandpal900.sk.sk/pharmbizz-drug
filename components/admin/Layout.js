import Head from 'next/head';

const Layout = (props) =>(
    <div>
        <Head>
            <meta charSet="utf-8" />
            <title>pharmbizz</title>

            {/* plugins:css */}
            
            <link rel="stylesheet" href="/static/admin/iconfonts/mdi/css/materialdesignicons.min.css" />
            <link rel="stylesheet" href="/static/admin/css/vendor.bundle.base.css" />
            <link rel="stylesheet" href="/static/admin/css/vendor.bundle.addons.css" />
            {/* endinject */}
            {/* <!-- inject:css --> */}
            <link rel="stylesheet" href="/static/admin/css/style.css" /> 
            {/* <!-- endinject --></link> */}

                        
            {/* container-scroller */}
            {/* <!-- plugins:js --> */}
            <script src="/static/admin/js/vendor.bundle.base.js"></script>
            <script src="/static/admin/js/vendor.bundle.addons.js"></script>
            {/* <!-- endinject --> */}
            {/* <!-- inject:js --> */}
            <script src="/static/admin/js/off-canvas.js"></script>
            <script src="/static/admin/js/misc.js"></script>
            {/* <!-- endinject --> */}
    
         </Head>
         {props.children}
         </div>
         );
         export default Layout;