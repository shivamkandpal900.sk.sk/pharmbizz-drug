const sidebar = (props) => (      
  <nav className="sidebar sidebar-offcanvas" id="sidebar">
  <ul className="nav">
    <li className="nav-item nav-profile"> 
      <div className="nav-link">
        <button className="btn btn-success btn-block">New Project
          <i className="mdi mdi-plus"></i>
        </button>
      </div>
    </li>
    <li className="nav-item">
      <a className="nav-link" href="../../index.html">
        <i className="menu-icon mdi mdi-television"></i>
        <span className="menu-title">Dashboard</span>
      </a>
    </li>
    <li className="nav-item">
      <a className="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
        <i className="menu-icon mdi mdi-content-copy"></i>
        <span className="menu-title">COUNTRY</span>
        <i className="menu-arrow"></i>
      </a>
      <div className="collapse" id="ui-basic">
        <ul className="nav flex-column sub-menu">
          <li className="nav-item">
            <a className="nav-link" href="#">1</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">2</a>
          </li>
        </ul>
      </div>
    </li>

    <li className="nav-item">
      <a className="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
        <i className="menu-icon mdi mdi-content-copy"></i>
        <span className="menu-title">DRUGS</span>
        <i className="menu-arrow"></i>
      </a>
      <div className="collapse" id="auth">
        <ul className="nav flex-column sub-menu">
          <li className="nav-item">
            <a className="nav-link" href="#"> 1 </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#"> 2 </a>
          </li>
        </ul>
      </div>
    </li>
  </ul>
</nav>
        );
        export default sidebar;  