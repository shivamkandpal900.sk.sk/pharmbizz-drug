const header = (props) => (
    <nav className="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    {/* NavBar */}
      <div className="navbar-brand-wrapper navbar-dark bg-dark">
        <a className="navbar-brand">
        <img src="/static/images/logos/pharmbizz-logo.png" alt="" />
        </a>
      </div>


      <div className="navbar-menu-wrapper d-flex align-items-center">

        <ul className="navbar-nav navbar-nav-right">

          
          <li className="nav-item dropdown d-none d-xl-inline-block">
            <a className="nav-link " id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
            </a>
            <div className="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a className="dropdown-item">
                Sign Out
              </a>
            </div>
          </li>
        </ul>  
      </div>
    </nav>
    );
    export default header;      