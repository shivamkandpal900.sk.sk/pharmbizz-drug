const Slider = () =>(
 <div> 
   <style jsx>{`
            .slide1{
                background: url('/static/images/news-banner/slider_img1.jpg') no-repeat;
            }
            .slide2{
              background: url('/static/images/news-banner/slider_img2.png') no-repeat;
            }
            .slide3{
              background: url('/static/images/news-banner/gth.jpg') no-repeat; 
            }
            .slide4{
              background: url('/static/images/news-banner/slider_img1.jpg') no-repeat; 
            }
            .slide5{
              background: url('/static/images/news-banner/slider_img2.png') no-repeat; 
            }
            .slide6{
              background: url('/static/images/news-banner/slider_img1.jpg') no-repeat; 
            }
        `}</style> 
{/* Slider */}
<div className="banner-slider-2 mt-20">
    <div className="container">
    <div className="row">
        <div className="col-sm-6 col-xs-12">
            <div className="banner-news-slider big-captions" id="banner-news-slider">
                <div className="news-item-holder">
                    <div className="row no-gutters">
                        <div className="col-xs-4">
                            <div className="post-thumb m-0 banner-slide-img-height overflow-hide slide1"  >
                                {/* image caption */}
                                <div className="thumb-over custom-size">
                                    <span>Pharma</span>
                                    <h3><a href="#">First line immunotherapy combination fails to improve overall survival in lung cancer</a></h3>
                                </div>
                                {/* image caption */}
                            </div>
                        </div>
                        <div className="col-xs-4">
                            <div className="post-thumb m-0 banner-slide-img-height overflow-hide slide2" >
                                {/* image caption */}
                                <div className="thumb-over custom-size">
                                    <span>Pharma</span>
                                    <h3><a href="#">First line immunotherapy combination fails to improve overall survival in lung cancer</a></h3>
                                </div>
                                {/* image caption */}
                            </div>
                        </div>
                        <div className="col-xs-4">
                            <div className="post-thumb m-0 banner-slide-img-height overflow-hide slide3">
                                {/* image caption */}
                                <div className="thumb-over custom-size">
                                    <span>Pharma</span>
                                    <h3><a href="#">First line immunotherapy combination fails to improve overall survival in lung cancer</a></h3>
                                </div>
                                {/* image caption */}
                            </div>
                        </div>
                     </div>   
                </div>
                <div className="news-item-holder">
                    <div className="row no-gutters">
                        <div className="col-xs-6">
                            <div className="post-thumb m-0 banner-slide-img-height overflow-hide slide4">
                                {/* image caption */}
                                <div className="thumb-over custom-size">
                                    <span>Pharma</span>
                                    <h3><a href="#">First line immunotherapy combination fails to improve overall survival in lung cancer</a></h3>
                                </div>
                                {/* image caption */}
                            </div>
                        </div>
                        <div className="col-xs-6">
                            <div className="post-thumb m-0 banner-slide-img-height overflow-hide slide5">
                                {/* image caption */}
                                <div className="thumb-over custom-size">
                                    <span>Pharma</span>
                                    <h3><a href="#">First line immunotherapy combination fails to improve overall survival in lung cancer</a></h3>
                                </div>
                                {/* image caption */}
                            </div>
                        </div>
                     </div>   
                </div>
                <div className="news-item-holder">
                    <div className="row no-gutters">
                        <div className="col-xs-12">
                            <div className="post-thumb m-0 banner-slide-img-height overflow-hide slide6">
                                {/* image caption */}
                                <div className="thumb-over custom-size">
                                    <span>Pharma</span>
                                    <h3><a href="#">First line immunotherapy combination fails to improve overall survival in lung cancer</a></h3>
                                </div>
                                {/* image caption */}
                            </div>
                        </div>
                     </div>   
                </div>
            </div>
        </div>
        <div className="col-sm-6 col-xs-12">
            <div className="row news-sections">
                <div className="col-sm-6 col-xs-4">
                    <div className="post-thumb m-0">
                        <img src="/static/images/news-banner/walmart.jpg" alt="" />

                         {/* image caption */}
                        <div className="thumb-over">
                            <h5><a href="#">Very little is needed to make a happy life it is all within yourself, in your way of thinking.</a></h5>
                            <ul className="post-meta">
                                <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                            </ul>
                        </div>
                        {/* image caption */}
                    </div>
                </div>
                
                <div className="col-sm-6 col-xs-4">
                    <div className="image">
                        <img src="/static/images/news-banner/stock.jpg" alt="" />
                         {/* image caption */}
                         {/* <div className="thumb-over">
                            <h5><a href="#">Very little is needed to make a happy life it is all within yourself, in your way of thinking.</a></h5>
                            <ul className="post-meta">
                                <li><i className="fa fa-clock-o"></i>25 dec, 2016</li>
                            </ul>
                        </div> */}
                        {/* image caption */}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    </div>
</div>
{/* Slider */}
</div>
	)

export default Slider;