import React from 'react';
import Link from 'next/link';
import Router from 'next/router';
import {FormGroup} from 'react-bootstrap';
import fetch from 'isomorphic-unfetch';
import {asyncContainer, Typeahead} from 'react-bootstrap-typeahead';
const AsyncTypeahead = asyncContainer(Typeahead);
const Fragment = React.Fragment;

class Search extends React.Component {
  static getInitialProps({ query }) {
    return { query }
  }
  handleClick(query){
    this.setState({query: query})
  }
  state = {
      isLoading: false,
      options: []
  };

  render() {
return (
  <Fragment>
<AsyncTypeahead
  isLoading={this.state.isLoading}
  onSearch={query => {
    this.setState({isLoading: true});
    fetch(`https://api-v2.intrinio.com/companies/search?query=${query}&api_key=OmY1YjdjNDQ1YTg2N2YxY2E3ZDQxYjhkM2UyNWIyZTgx`)
      .then(resp => resp.json())
      .then(json => this.setState({
        isLoading: false,
        options: json.companies,
      }));
  }}
  options={this.state.options}
  labelKey={option => `${option.ticker} ${option.name}`}
  renderMenuItemChildren={(option) => (
    <div onClick={(e)=>this.handleClick(option.ticker)}>
      <Link href={{ pathname: 'stock', query: { ticker: option.ticker }}}>
        <a> <span><b>{option.ticker}</b> {option.name}</span> </a>
      </Link> 
    </div> 
  )}
/>
</Fragment>
    )
  }
}
export default Search