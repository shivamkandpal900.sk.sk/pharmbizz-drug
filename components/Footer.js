import Link from 'next/link';

const Footer = () => (
	<div>
<footer className="footer">

 {/*  Footer Columns  */}
<div className="footer-columns">
<div className="container">
<div className="row position-r theme-padding">

 {/*  widget  */}
<div className="col-lg-3 col-sm-3 col-xs-6 r-full-width">
 {/*  Contact widget  */}
<div className="footer-widget">
<div className="contact-list">

 {/*  Logo Holder  */}
<a href="index.html" className="inner-logo-2"></a>
 {/*  Logo Holder  */}
<p></p>
</div>
</div>
 {/*  Contact widget  */}

</div>
 {/*  widget  */}

 {/*  widget  */}
<div className="col-lg-3 col-sm-3 col-xs-6 r-full-width">
 {/*  populer post   */}
<div className="footer-widget mb-30">
<h3>quick links</h3>
<div className="quick-links">
<ul className="footer-menu" styles={{width:'50%'}}>
<li><a href="#"> HOME</a></li>
<li><a href="#"> NEWS</a></li>
<li><a href="#"> STOCK</a></li>
<li><a href="#"> DRUGS</a></li>
</ul>
</div>
</div>
 {/*  populer post   */}
</div>
 {/*  widget  */}

 {/*  widget  */}
<div className="col-lg-3 col-sm-3 col-xs-6 r-full-width">
 {/*  quick links  */}
<div className="footer-widget mb-30">
<h3>Contact us</h3>
<div className="quick-links">
<p>123 Second Street Fifth Avenue,
<br /> 
Manhattan, New York 
<br /> 
<span styles={{fontSize:'18px'}}> 
   <a href="tel:+00412584896587">+00 41 258 489 6587</a> 
</span> 
<br /> 
<a href="emailto:info@demo.com">info@demo.com</a>
</p>

</div>
</div>
 {/*  quick links  */}

 {/*  populer tags  */}

 {/*  populer tags  */}
</div>
 {/*  widget  */}

 {/*  widget  */}
<div className="col-lg-3 col-sm-3 col-xs-6 r-full-width">

 {/*  Contact widget  */}

 {/*  Contact widget  */}

 {/*  Newsletter widget  */}
<div className="footer-widget">
<h3 className="mb-15">newsletter</h3>
<div className="newsletter-form">
<p>Enter your email here</p>
<form>
<div className="input-group">
<span className="input-group-addon"><i className="fa fa-envelope-o"></i></span>
<input type="text" className="form-control" placeholder="Your Email" />
<span className="input-group-btn">
<button type="submit" className="btn btn-icon">
<i className="fa fa-paper-plane"></i>
</button> 
</span>
</div>
</form>
</div>
</div>
 {/*  Newsletter widget  */}

</div>
 {/*  widget  */}

 {/*  back To Button  */}
<span id="scrollup" className="scrollup"><i className="fa fa-angle-up"></i></span>
 {/*  back To Button  */}

</div>
</div>
</div>
 {/*  Footer Columns  */}

 {/*  Copyright Bar  */}
<div className="sub-footer">
<div className="container">
<div className="copyright-bar">
<p>PHARMBIZZ <i className="fa fa-copyright"></i> 2018, All Rights Reserved</p>
<ul>
<li><a href="#"></a></li>
<li><a href="#"></a></li>
<li><a href="#"></a></li>
<li><a href="#"></a></li>
</ul>
</div>
</div>
</div>
 {/*  Copyright Bar  */}

 {/*  contact popup  */}
<div className="footer-contact-popup">
<div className="popup-wrap">
<a href="#" id="popup-btn"><i className="fa fa-envelope-o"></i></a> 
<div className="contact-holder">
<h4>Send us a message</h4>
<div className="contact-form">
<form>
<div className="form-group">
   <input type="text" placeholder="Your Name" className="form-control" />
</div>
<div className="form-group">
   <input type="email" placeholder="Your Email" className="form-control" />
</div>
<div className="form-group">
   <textarea className="form-control" placeholder="Message" rows="3"></textarea>
</div>
<a href="#" className="btn red full-width"><span>Send</span></a>
</form>
</div>
</div>
</div>
</div>
 {/*  contact popup  */}

</footer>
 {/*  Footer Starts  */}

</div>
	
	);

export default Footer;


