<footer id="footer" class="footer_main">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="footer_logo">
                     <img src="images/logos/pharmbizz-logo.png" alt="#" />
                  </div>
                  <p class="footer_desc">Investments and employment Blockchain Technologies. Optimize your business blockchain technology and Smart Contracts.</p>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="main-heading left_text">
                     <h2>Quick links</h2>
                  </div>
                  <ul class="footer-menu" style="width:50%;">
                     <li><a href="#"><i class="fa fa-angle-right"></i> Home</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> About</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Service</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Typography</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Partners</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Portfolio</a></li>
                  </ul>
                  <ul class="footer-menu" style="width:50%;">
                     <li><a href="#"><i class="fa fa-angle-right"></i> History</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Approach</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Careers</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Testimonials</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Team</a></li>
                     <li><a href="#"><i class="fa fa-angle-right"></i> Events</a></li>
                  </ul>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="main-heading left_text">
                     <h2>Contact us</h2>
                  </div>
                  <p>123 Second Street Fifth Avenue,<br>Manhattan, New York<br><span style="font-size:18px;"><a href="tel:+00412584896587">+00 41 258 489 6587</a></span><br><a href="emailto:info@demo.com">info@demo.com</a></p>
               </div>
               <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="main-heading left_text">
                     <h2>Newsletter Signup</h2>
                  </div>
                  <p style="font-size: 17px;line-height: 24px;margin: 0;letter-spacing: 0px;">Get latest updates, news, surveys & offers</p>
                  <div class="footer_mail-section" style="width: 90%;">
                     <form>
                        <fieldset>
                           <div class="field">
                              <input placeholder="Email" type="text">
                              <button class="button_custom"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                           </div>
                        </fieldset>
                     </form>
                  </div>
                  <ul class="social_icons">
                     <li class="social-icon fb"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li class="social-icon tw"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li class="social-icon gp"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
            </div>
         </div>
      </footer>
      <div class="footer-bottom">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 pull-left">
                  <p class="text-center">Pharmbizz. 2018 All Rights Reserved.</p>
               </div>
            </div>
         </div>
      </div>