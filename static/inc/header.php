<header id="default_header" class="header_style_1">
         <div class="header_top">
            <div class="container">
               <div class="row">
                  <div class="col-md-8 col-sm-8 col-xs-12">
                     
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="full">
                        <ul class="pull-right header_top_menu user_login">
                           <li><a href="#"><i class="fa fa-sign-in"></i> Login</a></li>
                           <li><a href="#"><i class="fa fa-lock"></i> Register</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="full">
                  <div class="col-md-3 col-sm-3 col-xs-12">
                     <!-- logo -->
                     <div class="logo">
                        <a href="index.html"><img class="img-responsive" src="images/logos/pharmbizz-logo.png" alt="logo" /></a>
                     </div>
                     <!-- end logo -->
                  </div>
                  <div class="col-md-8 col-sm-9 col-xs-12">
                     <!-- menu -->
                     <div class="main_menu">
                        <div id="cssmenu" class="dark_menu">
                           <ul>
                              <li>
                                 <a href="index.php">HOME</a>
                                 <!--<ul>
                                    <li><a href="index-2.html">Home Page light</a></li>
                                    <li><a href="home_dark.html">Home Page Dark</a></li>
                                 </ul>-->
                              </li>
                              <li><a href="#">NEWS</a></li>
                              <li><a href="stock.php">STOCK</a>
                                <!--<ul>
                                    <li><a href="#">Stock-H</a></li>
                                    <li><a href="#">Summary</a></li>
                                    <li><a href="#">Company</a></li>
                                    <li><a href="#">Ownership</a></li>
                                    <li><a href="#">Financial</a></li>
                                    <li><a href="#">Financial Balance Sheet</a></li>
                                    <li><a href="#">Analysis</a></li>
                                    <li><a href="#">Earnings</a></li>
                                    <li><a href="#">Short Interest</a></li>
                                 </ul>-->
                              </li>
                              <li><a href="#">DRUGS</a></li>
                              <li><a href="#">FUTURES</a></li>
                              <li><a href="#">CALENDER</a></li>
                              <li><a href="#">TOOLS</a></li>
                              <li><a href="#">MARKETPLACE</a></li>
                           </ul>
                        </div>
                     </div>
                     <!-- end menu -->
                  </div>
                  <div class="col-md-1 hidden-sm col-xs-12">
                     <div class="search_bar">
                        <button type="button" class="search_btn" data-toggle="modal" data-target="#search_form"><i class="fa fa-search" aria-hidden="true"></i></button>
                     </div>
                     <!-- end right header section -->
                  </div>
               </div>
            </div>
         </div>
      </header>