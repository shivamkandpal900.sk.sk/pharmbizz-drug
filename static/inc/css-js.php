<!-- Site Icons -->
<link rel="icon" href="images/fevicon/fav-icon.png" type="image/gif" />
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css" />
<!-- Site CSS -->
<link rel="stylesheet" href="css/style.css" />
<!-- Responsive CSS -->
<link rel="stylesheet" href="css/responsive.css" />
<!-- Colors CSS -->
<link rel="stylesheet" href="css/colors.css" />
<!-- Custom CSS -->
<link rel="stylesheet" href="css/custom.css" />
<!-- Counter CSS -->
<link rel="stylesheet" href="css/jquery.countdown.css" />
<!-- Wow Animation CSS -->
<link rel="stylesheet" href="css/animate.css" />
<!-- Market value slider CSS -->
<link rel="stylesheet" type="text/css" href="css/carouselTicker.css" media="screen" />
<link href="css/animate/3.5.2/animate.min.css" rel="stylesheet" media="all">
<!-- Custom CSS -->
<link rel="stylesheet" href="css/margin-padding.css" />
<link rel="stylesheet" type="text/css" href="css/slick.css">
<link rel="stylesheet" type="text/css" href="css/slick-theme.css">
<link rel="stylesheet" href="css/custom-style.css" />
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->